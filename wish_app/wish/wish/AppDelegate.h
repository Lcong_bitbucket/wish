//
//  AppDelegate.h
//  wish
//
//  Created by cong on 17/2/15.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CYLTabBarController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic,strong) CYLTabBarController *tabbarVC;


@end

