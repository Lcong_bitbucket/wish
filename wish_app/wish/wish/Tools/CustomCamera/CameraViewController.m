//
//  HomeViewController.m
//  LLSimpleCameraExample
//
//  Created by Ömer Faruk Gül on 29/10/14.
//  Copyright (c) 2014 Ömer Faruk Gül. All rights reserved.
//

#import "CameraViewController.h"
#import "ViewUtils.h"
#import "EditCameraViewController.h"

@interface CameraViewController ()
@property (strong, nonatomic) LLSimpleCamera *camera;
@property (strong, nonatomic) UIButton *snapButton;
@property (strong, nonatomic) UIButton *switchButton;
@property (strong, nonatomic) UIButton *flashButton;
@property (strong, nonatomic) UIButton *closeButton;
@property (strong, nonatomic) UIButton *photoButton;
@property (nonatomic,strong) NSMutableArray *selectedImageArray;
@property (nonatomic,strong) NSMutableArray *editFilterArray;

@end

@implementation CameraViewController

- (void)viewDidLoad {
    self.hiddenNavBarWhenPush = YES;
    [super viewDidLoad];
   
    _selectedImageArray = [NSMutableArray new];
    _editFilterArray = [[NSMutableArray alloc] init];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    // create camera vc
    self.camera = [[LLSimpleCamera alloc] initWithQuality:CameraQualityPhoto];
    
    // attach to the view and assign a delegate
    [self.camera attachToViewController:self withDelegate:self];
    
    // set the camera view frame to size and origin required for your app
    self.camera.view.frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
    
    // read: http://stackoverflow.com/questions/5427656/ios-uiimagepickercontroller-result-image-orientation-after-upload
    // you probably will want to set this to YES, if you are going view the image outside iOS.
    self.camera.fixOrientationAfterCapture = NO;
    
    
    // ----- camera buttons -------- //
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(0, 0, 44, 44);
    [self.closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [self.closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateSelected];
     [self.closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    
    
    self.photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.photoButton.frame = CGRectMake(0, 0, 44, 44);
    [self.photoButton setImage:[UIImage imageNamed:@"photo"] forState:UIControlStateNormal];
    [self.photoButton setImage:[UIImage imageNamed:@"photo"] forState:UIControlStateSelected];
     [self.photoButton addTarget:self action:@selector(photoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.photoButton];
    
    // snap button to capture image
    self.snapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.snapButton.frame = CGRectMake(0, 0, 70.0f, 70.0f);
    [self.snapButton setImage:[UIImage imageNamed:@"takecamera"] forState:UIControlStateNormal];
    [self.snapButton setImage:[UIImage imageNamed:@"takecamera"] forState:UIControlStateSelected];
      [self.snapButton addTarget:self action:@selector(snapButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.snapButton];
    
    // button to toggle flash
    self.flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashButton.frame = CGRectMake(0, 0, 44, 44);
    [self.flashButton setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
    [self.flashButton setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateSelected];
     [self.flashButton addTarget:self action:@selector(flashButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.flashButton];
    
    // button to toggle camera positions
    self.switchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.switchButton.frame = CGRectMake(0, 0, 44, 44);
    [self.switchButton setImage:[UIImage imageNamed:@"switch"] forState:UIControlStateSelected];
     [self.switchButton setImage:[UIImage imageNamed:@"switch"] forState:UIControlStateNormal];
     [self.switchButton addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.switchButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    // start the camera
    [self.camera start];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

}

/* camera buttons */
- (void)switchButtonPressed:(UIButton *)button {
    [self.camera togglePosition];
}

- (void)flashButtonPressed:(UIButton *)button {
    
    CameraFlash flash = [self.camera toggleFlash];
    if(flash == CameraFlashOn) {
        self.flashButton.selected = YES;
    }
    else {
        self.flashButton.selected = NO;
    }
}

- (void)photoButtonPressed:(UIButton*)button{
    
}

- (void)closeButtonPressed:(UIButton*)button{
    [self goBack];
}

- (void)snapButtonPressed:(UIButton *)button {
    
    // capture the image, delegate will be executed
    [self.camera capture];
}

/* camera delegates */
- (void)cameraViewController:(LLSimpleCamera *)cameraVC didCaptureImage:(UIImage *)image {
    [_selectedImageArray addObject:image];
    [_editFilterArray addObject:image];

    // we should stop the camera, since we don't need it anymore. We will open a new vc.
    [self.camera stop];
    EditCameraViewController *vc = [[EditCameraViewController alloc] init];
    vc.originalImageArray = _selectedImageArray;
    vc.editFilterArray = _editFilterArray;
    [self.navigationController pushViewController:vc animated:YES];
    

}

- (void)cameraViewController:(LLSimpleCamera *)cameraVC didChangeDevice:(AVCaptureDevice *)device {
    
    // device changed, check if flash is available
    if(cameraVC.isFlashAvailable) {
        self.flashButton.hidden = NO;
    }
    else {
        self.flashButton.hidden = YES;
    }
    
    self.flashButton.selected = NO;
}

/* other lifecycle methods */
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.camera.view.frame = self.view.contentBounds;
    
    self.snapButton.center = self.view.contentCenter;
    self.snapButton.bottom = self.view.height - 15;
    
    self.flashButton.center = self.view.contentCenter;
    self.flashButton.top = 5.0f;
    
    self.switchButton.top = 5.0f;
    self.switchButton.right = self.view.width - 5.0f;
    
    self.closeButton.top = 5.0f;
    self.closeButton.left = 15;
    
    self.photoButton.left = 20;
    self.photoButton.bottom = self.view.height - 30;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
