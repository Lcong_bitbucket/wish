//
//  EditCameraViewController.m
//  wish
//
//  Created by cong on 17/3/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "EditCameraViewController.h"
//#import "FeSlideFilterView.h"
//#import "CIFilter+LUT.h"
#import "UIButton+ImageTitleSpacing.h"
#import "DynamicPublishViewController.h"
#import "UIImage+AddFunction.h"

#import "ImageUtil.h"
#import "ColorMatrix.h"
#import "YBPasterView.h"

@interface EditCameraViewController ()<YBPasterViewDelegate>
{
    NSInteger _selectedImageIndex;//当前选中的原图
    NSInteger _selectedFilterIndex;//当前选中的滤镜

    BOOL _isSelectedFilter;
}
@property (strong, nonatomic) UIImageView *curEditImageView;//当前编辑图
@property (strong, nonatomic) NSMutableArray *arrPhoto;

@property (strong, nonatomic) NSArray *arrTittleFilter;//滤镜效果
//@property (strong, nonatomic) FeSlideFilterView *filterView;


@property (strong, nonatomic) NSArray *bottomArray1;//
//@property (strong, nonatomic) NSArray *bottomArray2;//


@end

@implementation EditCameraViewController

- (void)viewDidLoad {
    self.hiddenNavBarWhenPush = YES;
    [super viewDidLoad];
    
    _arrTittleFilter = @[@"原图",@"LOMO",@"黑白",@"复古",@"哥特",@"瑞华",@"淡雅",@"酒红",@"青柠",@"浪漫",@"光晕",@"蓝调",@"梦幻",@"夜色"];
    _bottomArray1 = @[@{@"title":@"滤镜",@"icon":@"bt1"},@{@"title":@"特效1",@"icon":@"bt1"},@{@"title":@"特效1",@"icon":@"bt1"},@{@"title":@"特效1",@"icon":@"bt1"},@{@"title":@"特效1",@"icon":@"bt1"}];

    _curEditImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,55 , Screen_Width, Screen_Height - 55 - 60)];
    [self.view addSubview:_curEditImageView];
    
    _curEditImageView.image = _editFilterArray[_selectedImageIndex];
    
    // Do any additional setup after loading the view from its nib.
//     _isSelectedFilter = YES;
    [self refreshUI];
    [self refreshTop];
}

- (void)refreshTop{
    for(int i = 0; i < _editFilterArray.count;i ++){
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10*(i+1) + 40*i, 5, 40, 40)];
         [btn setBackgroundImage:_editFilterArray[i] forState:UIControlStateNormal];
        btn.tag = 10 + i;
        [btn addTarget:self action:@selector(topSelected:) forControlEvents:UIControlEventTouchUpInside];
        [_topScroll addSubview:btn];
    }
    [_topScroll setContentSize:CGSizeMake(55*_editFilterArray.count, 40)];
}

- (void)topSelected:(UIButton *)btn{
    _selectedImageIndex = btn.tag - 10;
    _selectedFilterIndex = 0;
    _isSelectedFilter = NO;
    _curEditImageView.image = _editFilterArray[_selectedImageIndex];
    [self refreshUI];
}

- (void)refreshUI{
    showHUDCustomView(self.view, @"加载中...", nil, 0);

    [_bottomScroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if(!_isSelectedFilter){
        for(int i = 0; i < _bottomArray1.count;i ++){
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10*(i+1) + 50*i, 5, 50, 50)];
            [btn setTitle:_bottomArray1[i][@"title"] forState:UIControlStateNormal];
            [btn setImage:[UIImage imageNamed:_bottomArray1[i][@"icon"]] forState:UIControlStateNormal];
            [btn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:5];
            btn.tag = 20+i;
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
            [btn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(bottomSelected1:) forControlEvents:UIControlEventTouchUpInside];
            [_bottomScroll addSubview:btn];
        }
        [_bottomScroll setContentSize:CGSizeMake(65*_bottomArray1.count, 50)];
    } else {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, 30, 50)];
        [btn setImage:[UIImage imageNamed:@"back-arrow"] forState:UIControlStateNormal];
        [btn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:5];
        btn.tag = 29;
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
         [btn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
         [btn addTarget:self action:@selector(backClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomScroll addSubview:btn];
        
        for(int i = 0; i < _arrTittleFilter.count;i ++){
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(40+ 10*(i+1) + 50*i, 5, 50, 50)];
            [btn setTitle:_arrTittleFilter[i] forState:UIControlStateNormal];
            [btn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:5];
            btn.tag = 30+i;
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
             [btn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(bottomSelected2:) forControlEvents:UIControlEventTouchUpInside];
            [_bottomScroll addSubview:btn];
            
        }
        [_bottomScroll setContentSize:CGSizeMake(40+ 10*(_arrTittleFilter.count+1) + 50*(_arrTittleFilter.count+1), 50)];
     }
    hidHUDCustomView(self.view);

}

/**
 *  设置button的预览图
 */
- (UIImage *)buttonSetImageWithButton:(NSInteger )index
{
    UIImage *originImage = _originalImageArray[_selectedImageIndex];
    NSInteger currentIndex = index;
    UIImage *buttonImage;
    switch (currentIndex)
    {
        case 0:
        {
            buttonImage = originImage;
        }
            break;
        case 1:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_lomo];
        }
            break;
        case 2:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_heibai];
        }
            break;
        case 3:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_huajiu];
        }
            break;
        case 4:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_gete];
        }
            break;
        case 5:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_ruise];
        }
            break;
        case 6:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_danya];
        }
            break;
        case 7:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_jiuhong];
        }
            break;
        case 8:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_qingning];
        }
            break;
        case 9:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_langman];
        }
            break;
        case 10:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_guangyun];
        }
            break;
        case 11:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_landiao];
        }
            break;
        case 12:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_menghuan];
        }
            break;
        case 13:
        {
            buttonImage = [ImageUtil imageWithImage:originImage withColorMatrix:colormatrix_yese];
        }
            break;
            
        default:
            break;
    }
    
    return buttonImage;
}

- (UIImage *)doneEdit:(NSInteger )index
{
    CGFloat org_width = self.curEditImageView.image.size.width ;
    CGFloat org_heigh = self.curEditImageView.image.size.height ;
    CGFloat rateOfScreen = org_width / org_heigh ;
    
     CGRect rect = CGRectZero ;
    
    rect.size = CGSizeMake(40, 40 / rateOfScreen) ;
    rect.origin = CGPointMake(0, (40 - 40 / rateOfScreen) / 2) ;
    
    UIImage *imgTemp = _editFilterArray[index] ;
    UIImage *imgCut = [UIImage squareImageFromImage:imgTemp scaledToSize:rect.size.width] ;
    
    return imgCut ;
}
 
- (void)bottomSelected1:(UIButton *)btn{
    if(btn.tag - 20 == 0){
        _isSelectedFilter = YES;
         [self refreshUI];
    } else {
        //标签选择
//        YBPasterView *pasterView = [[YBPasterView alloc]initWithFrame:_curEditImageView.bounds];
//        pasterView.pasterImage = _curEditImageView.image;
//        pasterView.delegate = self;
//        [_curEditImageView addSubview:pasterView];
    }
}


#pragma mark - YBPasterViewDelegate
- (void)deleteThePaster
{
    
}

- (void)backClick:(UIButton *)btn{
    _isSelectedFilter = NO;
    [self refreshUI];
}

- (void)bottomSelected2:(UIButton *)btn{
 
    _selectedFilterIndex = btn.tag - 30;
    [btn setBackgroundImage:[self buttonSetImageWithButton:_selectedFilterIndex] forState:UIControlStateNormal];

    [_editFilterArray replaceObjectAtIndex:_selectedImageIndex withObject:[self buttonSetImageWithButton:_selectedFilterIndex]];
    _curEditImageView.image = _editFilterArray[_selectedImageIndex];

}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
  
    // start the camera
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

  /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self goBack];
}

- (IBAction)nextStep:(id)sender {
    DynamicPublishViewController *vc = [[DynamicPublishViewController alloc] init];
    vc.editFilterArray = self.editFilterArray;
    [self.navigationController pushViewController:vc animated:YES];
    
}
@end
