//
//  EditCameraViewController.h
//  wish
//
//  Created by cong on 17/3/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
//#import "FeSlideFilterView.h"

@interface EditCameraViewController : XCBaseViewController
- (IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *topScroll;
- (IBAction)nextStep:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *bottomScroll;


@property (nonatomic,strong) NSMutableArray *originalImageArray;//原图
@property (strong, nonatomic) NSMutableArray *editFilterArray;//编辑效果
@end
