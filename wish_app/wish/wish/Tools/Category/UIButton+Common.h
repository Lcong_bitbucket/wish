//
//  UIButton+Common.h
//  xcwl
//
//  Created by 聪 on 16/4/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+WebCache.h"

typedef void(^CompletionBlock)(UIImage *image, NSError *error);

@interface UIButton (Common)
- (void)setImageWithURLString:(NSString *)urlString forState:(UIControlState)state placeholderImage:(UIImage *)placeholder;
- (void)setBackgroundImageWithURLString:(NSString *)url forState:(UIControlState)state placeholderImage:(UIImage *)placeholder ;
- (void)setImageWithURLString:(NSString *)urlString forState:(UIControlState)state placeholderImage:(UIImage *)placeholder completed:(CompletionBlock)completedBlock;
- (void)setBackgroundImageWithURLString:(NSString *)urlString forState:(UIControlState)state placeholderImage:(UIImage *)placeholder completed:(CompletionBlock)completedBlock;
@end
