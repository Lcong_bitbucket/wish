//
//  UIImageView+Common.h
//  xcwl
//
//  Created by 聪 on 16/4/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
typedef void(^CompletionBlock)(UIImage *image, NSError *error);

@interface UIImageView (Common)
- (void)setImageURLStr:(NSString *)urlString placeholder:(UIImage *)image;
- (void)setImageWithURLStr:(NSString *)urlString placeholderImage:(UIImage *)placeholder completed:(CompletionBlock)completedBlock;
@end
