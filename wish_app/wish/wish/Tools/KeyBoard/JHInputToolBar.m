//
//  JHToolBar.m
//  emotions
//
//  Created by zhou on 16/7/5.
//  Copyright © 2016年 zhou. All rights reserved.
//

#import "JHInputToolBar.h"
#import "UIView+Extension.h"

@interface JHInputToolBar ()<UITextViewDelegate>

@property (nonatomic, strong) UIButton *emotionButton;
@property (nonatomic, strong) UIButton *sendBtn;
@property (nonatomic, strong) UIButton *picBtn;

@end

@implementation JHInputToolBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat btnW = 44;
        
        self.backgroundColor = UIColorFromRGB(0xefefef);
        
        self.emotionButton = [[UIButton alloc]init];
        [self.emotionButton setImage:[UIImage imageNamed:@"compose_emoticonbutton_background"] forState:UIControlStateNormal];
        [self.emotionButton setImage:[UIImage imageNamed:@"compose_emoticonbutton_background_highlighted"] forState:UIControlStateHighlighted];
        [self.emotionButton addTarget:self action:@selector(emotionClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.emotionButton];
        
        self.picBtn = [[UIButton alloc]init];
        [self.picBtn setImage:[UIImage imageNamed:@"compose_toolbar_picture"] forState:UIControlStateNormal];
        [self.picBtn setImage:[UIImage imageNamed:@"compose_toolbar_picture_highlighted"] forState:UIControlStateHighlighted];
        [self.picBtn addTarget:self action:@selector(picClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.picBtn];
        self.picBtn.hidden = YES;
        
        self.textView = [[JHEmotionTextView alloc] initWithFrame:CGRectMake(8, 5, Screen_Width - 100, self.mj_h)];
        _textView.placeholder = @"说点什么吧...";
        _textView.delegate = self;
        WEAKSELF;
        [_textView setTextChangeBlock:^(NSString *fullText) {
            if(fullText.length == 0){
                weakSelf.sendBtn.selected = NO;
                weakSelf.sendBtn.enabled = NO;
            } else {
                weakSelf.sendBtn.selected = YES;
                weakSelf.sendBtn.enabled = YES;
            }
        }];
        [self addSubview:self.textView];
        
        self.sendBtn = [[UIButton alloc]init];
        [self.sendBtn setImage:[UIImage imageNamed:@"sendComment"] forState:UIControlStateNormal];
        [self.sendBtn setImage:[UIImage imageNamed:@"sendComment_s"] forState:UIControlStateSelected];
        [self.sendBtn addTarget:self action:@selector(sendClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.sendBtn];
        self.sendBtn.selected = NO;
        self.sendBtn.enabled = NO;
        
        
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(8);
            make.top.mas_equalTo(8);
            make.right.mas_equalTo(self.emotionButton.mas_left).offset(-5);
            make.bottom.mas_equalTo(-8);
        }];
        
        [self.emotionButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.sendBtn.mas_left).offset(-5);
            make.centerY.mas_equalTo(self.textView.mas_centerY);
            make.width.mas_equalTo(btnW);
            make.height.mas_equalTo(btnW);
        }];
        
        [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.textView.mas_centerY);
            make.right.mas_equalTo(-8);
            make.width.mas_equalTo(btnW);
            make.height.mas_equalTo(btnW);
        }];
        
        //处理表情选中的通知
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(emotionDidSelect:) name:@"JHEmotionDidSelectNotification" object:nil];
        //删除文字
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(emotionDidDelete) name:@"JHEmotionDidDeleteNotification" object:nil];
 
    }
    
    return self;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

/**
 *  删除文字
 */
- (void)emotionDidDelete
{
    [self.textView deleteBackward];
}
/**
 *  表情被选中了
 */
- (void)emotionDidSelect:(NSNotification *)notification
{
    JHEmotion *emotion = notification.userInfo[@"JHSelectEmotionKey"];
    [self.textView insertEmotion:emotion];
}

- (void)picClick:(UIButton*)btn{
    if ([self.delegate respondsToSelector:@selector(toolBar:ButtonTag:andTextView:)]) {
        [self.delegate toolBar:self ButtonTag:1 andTextView:self.textView];
    }
}

- (void)emotionClick:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(toolBar:ButtonTag:andTextView:)]) {
        
        [self.delegate toolBar:self ButtonTag:0 andTextView:self.textView];
     
    }
}

- (void)sendClick:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(toolBar:ButtonTag:andTextView:)]) {
        [self.delegate toolBar:self ButtonTag:2 andTextView:self.textView];
        self.textView.text = @"";
        [self.textView resignFirstResponder];
        self.sendBtn.selected = NO;
        self.sendBtn.enabled = NO;
    }
}

/**
 *  showKeyboardButton的set方法，判断键盘是自定义的键盘 还是 系统的键盘，来分别显示不同的图片样式
 *
 *  @param showKeyboardButton showKeyboardButton
 */
- (void)setShowKeyboardButton:(BOOL)showKeyboardButton
{
    _showKeyboardButton = showKeyboardButton;
    
    // 默认的图片名
    NSString *image = @"compose_emoticonbutton_background";
    NSString *highImage = @"compose_emoticonbutton_background_highlighted";
    
    // 显示键盘图标
    if (showKeyboardButton) {
        image = @"compose_keyboardbutton_background";
        highImage = @"compose_keyboardbutton_background_highlighted";
    }

    // 设置图片
    [self.emotionButton setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [self.emotionButton setImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
}
///**
// *  布局
// */
//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    
//    NSUInteger count = self.subviews.count;
//    CGFloat btnW = self.width/count;
//    CGFloat btnH = self.height;
//    for (NSUInteger i = 0; i < count; i++) {
//        UIButton *btn = self.subviews[i];
//        btn.y = 0;
//        btn.width = btnW;
//        btn.height = btnH;
//        btn.x = i * btnW;
//    }
//}

@end
