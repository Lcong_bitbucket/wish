//
//  JHToolBar.h
//  emotions
//
//  Created by zhou on 16/7/5.
//  Copyright © 2016年 zhou. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JHInputToolBar;
#import "JHEmotionTextView.h"
@protocol JHInputToolBarDelegate <NSObject>

@optional
- (void)toolBar:(JHInputToolBar *)toolBar ButtonTag:(NSUInteger)tag andTextView:(JHEmotionTextView *)textView;

@end

@interface JHInputToolBar : UIView

@property (nonatomic,weak) id<JHInputToolBarDelegate> delegate;
@property (nonatomic, strong) JHEmotionTextView *textView;

@property (nonatomic,assign) BOOL showKeyboardButton;
@property (nonatomic,assign) NSInteger replyTag;//第几个朋友圈说说
@end
