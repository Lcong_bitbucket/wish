//
//  CommonUtility.h
//  SandBayCinema
//
//  Created by Rayco on 12-11-1.
//  Copyright (c) 2012年 Apps123. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "UIImage+GIF.h"


CG_INLINE void setViewFrameSizeWidth(UIView *v ,float w) {
    CGRect frame = v.frame;
    frame.size.width = w;
    v.frame = frame;
}

CG_INLINE AppDelegate* app_delegate() {
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

CG_INLINE UIColor *getHexColor(NSString *hexColor) {
    if(!hexColor || [hexColor isEqualToString:@""] || [hexColor length] < 7){
        if (hexColor.length != 4) {
            return [UIColor whiteColor];
        }
    }
    
    if (hexColor.length == 4) {
        hexColor = [NSString stringWithFormat:@"#%c%c%c%c%c%c",[hexColor characterAtIndex:1],[hexColor characterAtIndex:1],[hexColor characterAtIndex:2],[hexColor characterAtIndex:2],[hexColor characterAtIndex:3],[hexColor characterAtIndex:3]];
    }
    
    unsigned int red,green,blue;
    NSRange range;
    range.length = 2;
    
    range.location = 1;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
    
    range.location = 3;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
    
    range.location = 5;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}
CG_INLINE CGSize GetStringSize(NSString *str,float fontSize,NSString *fontName,CGSize formatSize) {
    UIFont *font;
    if (fontName && fontName.length > 1) {
        font = [UIFont fontWithName:fontName size:fontSize];
    }
    else {
        font = [UIFont systemFontOfSize:fontSize];
    }
    
    CGSize size = formatSize;
    CGSize labelsize = CGSizeZero;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
        labelsize = [str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    }
    else {
        labelsize = [str sizeWithFont:font
                    constrainedToSize:size
                        lineBreakMode:NSLineBreakByWordWrapping];
    }
    
    return labelsize;
}

CG_INLINE BOOL isValidString(id obj) {
    if (!obj) {
        return NO;
    }
    if ([obj isKindOfClass:NSClassFromString(@"NSString")]) {
        if ([obj length] > 0) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - MBHUD
CG_INLINE void showHint(NSString *hint){
    if(hint.length == 0 || !hint){
        return;
    }
    UIView *view = [[UIApplication sharedApplication].delegate window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.userInteractionEnabled = NO;
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = hint;
    hud.margin = 10.f;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    //    hud.yOffset = iPhone5()?200.f:150.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:3];
}

CG_INLINE void showHUD(NSString *hint,UIView *view){
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
    HUD.labelText = hint;
    [view addSubview:HUD];
    HUD.bezelView.backgroundColor = [UIColor blackColor];
    HUD.contentColor = [UIColor whiteColor];
    [HUD show:YES];
}

CG_INLINE void showHUDCustomView(UIView *view,NSString *hint,NSString *imgName,NSInteger delay){
    // 显示到主窗口中
    UIImage  *image=[UIImage sd_animatedGIFNamed:imgName];
    UIImageView  *gifview=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,image.size.width, image.size.height)];
    gifview.image=image;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.color=[UIColor clearColor];//默认颜色太深了
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = hint;
    hud.customView=gifview;
    if(delay)
        [hud hide:YES afterDelay:delay];
}



CG_INLINE void showHUDDefaultCustomView(UIView *view,NSString *hint){
    showHUDCustomView(view,hint,@"loading1",0);
}

CG_INLINE void hidHUDCustomView(UIView *view){
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}

CG_INLINE void openWithMJPhoto(NSInteger index,NSMutableArray *imageArray,UIImageView *iv){
    // 1.封装图片数据
    NSMutableArray *photos = [NSMutableArray new];
    for (int i=0; i<imageArray.count; i++) {
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.srcImageView = iv; // 来源于哪个UIImageView
        photo.url = [NSURL URLWithString:imageArray[i]];
        [photos addObject:photo];
    }
    
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = index; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    browser.showSaveBtn = YES;
    [browser show];
}


#pragma mark - webview
CG_INLINE NSMutableArray * setDefaultJSEvent(id webView){
    //添加长按
    //    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    //    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    
    
    if([webView isKindOfClass:[WKWebView class]]){
        //获取图片数组
        static  NSString * const jsGetImages = @"function getImages(){\
        var objs = document.getElementsByTagName(\"img\");\
        var imgScr = '';\
        for(var i=0;i<objs.length;i++){\
        imgScr = imgScr + objs[i].src + '+';\
        };\
        return imgScr;\
        };";
        [webView evaluateJavaScript:jsGetImages completionHandler:^(id result, NSError * _Nullable error) {
            
        }];
        __block NSMutableArray *imageArr = [NSMutableArray new];
        [webView evaluateJavaScript:@"getImages()" completionHandler:^(id urlResurlt, NSError * _Nullable error) {
            [imageArr addObjectsFromArray:[urlResurlt componentsSeparatedByString:@"+"]];
            if (imageArr.count >= 2) {
                [imageArr removeLastObject];
            }
        }];
        
        [webView evaluateJavaScript:@"function registerImageClickAction(){\
         var imgs=document.getElementsByTagName('img');\
         var length=imgs.length;\
         for(var i=0;i<length;i++){\
         img=imgs[i];\
         var p = img.parentNode;\
         var tag = p.tagName;\
         if(tag .toUpperCase() != 'A'){\
         img.onclick=function(){\
         window.location.href='xcshowimg:'+this.src}\
         }\
         }\
         }" completionHandler:^(id result, NSError * _Nullable error) {
             
         }];
        
        [webView evaluateJavaScript:@"registerImageClickAction();" completionHandler:^(id result, NSError * _Nullable error) {
            
        }];
        return imageArr;
        
    } else {
        //获取图片数组
        static  NSString * const jsGetImages = @"function getImages(){\
        var objs = document.getElementsByTagName(\"img\");\
        var imgScr = '';\
        for(var i=0;i<objs.length;i++){\
        imgScr = imgScr + objs[i].src + '+';\
        };\
        return imgScr;\
        };";
        [webView stringByEvaluatingJavaScriptFromString:jsGetImages];//注入js方法
        NSString *urlResurlt = [webView stringByEvaluatingJavaScriptFromString:@"getImages()"];
        NSMutableArray* imageArray = [NSMutableArray arrayWithArray:[urlResurlt componentsSeparatedByString:@"+"]];
        if (imageArray.count >= 2) {
            [imageArray removeLastObject];
        }
        
        
        //添加图片可点击js
        [webView stringByEvaluatingJavaScriptFromString:@"function registerImageClickAction(){\
         var imgs=document.getElementsByTagName('img');\
         var length=imgs.length;\
         for(var i=0;i<length;i++){\
         img=imgs[i];\
         var p = img.parentNode;\
         var tag = p.tagName;\
         if(tag .toUpperCase() != 'A'){\
         img.onclick=function(){\
         window.location.href='xcshowimg:'+this.src}\
         }\
         }\
         }"];
        [webView stringByEvaluatingJavaScriptFromString:@"registerImageClickAction();"];
        
        return imageArray;
    }
}
CG_INLINE void openTel(NSString *urlStr){
    
    NSString *tel = urlStr;
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",tel];
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:str]])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    
}

//0不跳转，1跳site_url，2跳content，3项目，4需求， 5资讯，6发布项目，7发布需求 8我要咨询（项目或需求）　9我要咨询机构　10预约专家　11我的消息　12专家　13机构 14发布专家　　15发布机构

CG_INLINE void goADPage(NSDictionary *dic,UIViewController *pushvc){
    
    UINavigationController *nav ;
    UIViewController *rootVC = [[[[UIApplication sharedApplication]delegate]window]rootViewController];
    __block  UIViewController *nextVc = nil;
    

    if(nextVc){
        nextVc.hidesBottomBarWhenPushed=YES;

        if(pushvc == nil){
            nav = [[UINavigationController alloc] initWithRootViewController:nextVc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:nextVc animated:YES];
        }
    }
   
}
