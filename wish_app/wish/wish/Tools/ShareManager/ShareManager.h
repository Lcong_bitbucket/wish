//
//  ShareManager.h
//  GolfFriend
//
//  Created by Vescky on 14-5-18.
//  Copyright (c) 2014年 vescky.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UMSocialCore/UMSocialCore.h>
#import <UShareUI/UShareUI.h>

@interface ShareManager : NSObject {
    
}
@property (nonatomic,strong) UIImageView *shareIV;
+ (id)shareManeger;
//默认所有平台
- (void)shareDic:(NSDictionary *)dic vc:(UIViewController *)vc;
//指定平台 不需要弹框
- (void)sharePlatformName:(UMSocialPlatformType)platformName Dic:(NSDictionary *)dic;
@end
