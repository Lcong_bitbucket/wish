//
//  ZOHotSearchStyles.h
//  ZUIIO
//
//  Created by mingdchan on 16/9/8.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "AVObject+Subclass.h"

@interface ZOHotSearchStyles : AVObject <AVSubclassing>

@property (nonatomic, strong) NSArray *styles;

@end
