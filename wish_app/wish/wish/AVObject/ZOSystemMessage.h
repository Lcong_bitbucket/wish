//
//  ZOSystemMessage.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/6.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "AVObject+Subclass.h"

@interface ZOSystemMessage : AVObject <AVSubclassing>

@property (nonatomic, copy) NSString *messageTitle;
@property (nonatomic, copy) NSString *messageDetail;
@property (nonatomic, copy) NSString *messageUrl;
@property (nonatomic, strong) AVFile *messageImage;

@end
