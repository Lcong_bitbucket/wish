//
//  ZOSpaceType.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZOSpaceType : AVObject<AVSubclassing>

@property (nonatomic, assign) int type;

@property (nonatomic, strong) NSString *spaceType;

@end
