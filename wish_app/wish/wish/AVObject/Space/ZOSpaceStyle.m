//
//  ZOSpaceStyle.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOSpaceStyle.h"

@implementation ZOSpaceStyle

@dynamic style;
@dynamic spaceStyle;

+ (NSString *)parseClassName{
    return @"SpaceStyle";
}

@end
