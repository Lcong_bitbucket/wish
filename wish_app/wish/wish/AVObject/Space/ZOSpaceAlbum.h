//
//  ZOSpaceAlbum.h
//  ZUIIO
//
//  Created by mingdchan on 16/7/29.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "ZOSpace.h"

@interface ZOSpaceAlbum : AVObject<AVSubclassing>

@property (nonatomic,strong) AVUser *creator;

@property (nonatomic,strong) ZOSpace *space;

@property (nonatomic,copy)  NSString* albumContent;

@property (nonatomic,strong)  NSArray* digUsers;

@property (nonatomic,strong)  NSArray* albumPhotos;

@property (nonatomic,strong)  NSArray* photoSizes;

@end
