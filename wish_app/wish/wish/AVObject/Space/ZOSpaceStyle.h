//
//  ZOSpaceStyle.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZOSpaceStyle : AVObject<AVSubclassing>

@property (nonatomic, assign) int style;

@property (nonatomic, strong) NSString *spaceStyle;

@end
