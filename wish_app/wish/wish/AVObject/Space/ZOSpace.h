//
//  ZOSpace.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "ZOCBD.h"
#import "ZOSpaceType.h"
#import "ZOSpacePurpose.h"
#import "AVObject+Subclass.h"

#define SPACE_KEY_GALLERY @"gallery"
#define SPACE_KEY_LOCATION @"location"
#define SPACE_KEY_SpaceStyle @"spaceStyle"
#define SPACE_KEY_SpaceType @"spaceType"
#define SPACE_KEY_CBD @"CBD"

@interface ZOSpace : AVObject <AVSubclassing>

@property (nonatomic, strong) AVFile   *hostAvatar;
@property (nonatomic, strong) NSString *hostPhoneNum;
@property (nonatomic, strong) NSString *hostName;
@property (nonatomic, strong) NSString *hostBrief;
@property (nonatomic, strong) NSString *hostGender;

@property (nonatomic, strong) NSString *spaceName;
@property (nonatomic, strong) NSString *spaceBrief;
@property (nonatomic, strong) NSString *spaceInfo;
@property (nonatomic, strong) NSString *spaceAddress;
@property (nonatomic, strong) NSString *openingTime;
@property (nonatomic, strong) AVGeoPoint *location;

@property (nonatomic, strong) NSString *serviceId;

@property (nonatomic, strong) ZOCBD *CBD;
@property (nonatomic, strong) ZOSpaceType *spaceType;
@property (nonatomic, strong) ZOSpacePurpose *spacePurpose;
@property (nonatomic, strong) NSArray  *spaceStyle;

@property (nonatomic, strong) AVFile   *headPic;
@property (nonatomic, strong) AVFile   *middlePic;
@property (nonatomic, strong) NSArray  *gallery;

@property (nonatomic, strong) NSArray  *galleryInfo;
@property (nonatomic, assign) int views;
@property (nonatomic, assign) int favoriteTimes;
@property (nonatomic, assign) float spaceRank;

@property (nonatomic, assign) BOOL isRemoved;//下架

@end
