//
//  ZOCBD.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZOCBD : AVObject<AVSubclassing>

@property (nonatomic, copy) NSString *CBD;
@property (nonatomic, copy) NSString *cbdBrief;
@property (nonatomic, copy) NSString *cbdPicUrl;
@property (nonatomic, strong) NSNumber *views;

@end
