//
//  ZOSpace.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOSpace.h"

@implementation ZOSpace

@dynamic hostAvatar;
@dynamic hostPhoneNum;
@dynamic hostName;
@dynamic hostBrief;
@dynamic hostGender;

@dynamic spaceName;
@dynamic spaceBrief;
@dynamic spaceInfo;
@dynamic spaceAddress;
@dynamic openingTime;
@dynamic location;


@dynamic serviceId;

@dynamic CBD;
@dynamic spaceType;
@dynamic spacePurpose;
@dynamic spaceStyle;

@dynamic headPic;
@dynamic middlePic;
@dynamic gallery;

@dynamic galleryInfo;
@dynamic views;
@dynamic favoriteTimes;
@dynamic spaceRank;

@dynamic isRemoved;

+ (NSString *)parseClassName{
    return @"Space";
}

@end
