//
//  ZOSpaceAlbum.m
//  ZUIIO
//
//  Created by mingdchan on 16/7/29.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOSpaceAlbum.h"

@implementation ZOSpaceAlbum

@dynamic creator;
@dynamic space;
@dynamic albumContent;
@dynamic albumPhotos;
@dynamic digUsers;
@dynamic photoSizes;

+ (NSString *)parseClassName{
    return @"SpaceAlbum";
}

@end
