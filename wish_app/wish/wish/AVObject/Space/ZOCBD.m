//
//  ZOCBD.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOCBD.h"

@implementation ZOCBD

@dynamic CBD;
@dynamic cbdBrief;
@dynamic cbdPicUrl;
@dynamic views;

+ (NSString *)parseClassName{
    return @"CBD";
}

@end
