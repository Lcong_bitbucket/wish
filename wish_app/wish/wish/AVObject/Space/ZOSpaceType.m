//
//  ZOSpaceType.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOSpaceType.h"

@implementation ZOSpaceType

@dynamic type;
@dynamic spaceType;

+ (NSString *)parseClassName{
    return @"SpaceType";
}

@end
