//
//  ZOSpacePurpose.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOSpacePurpose.h"

@implementation ZOSpacePurpose

@dynamic type;
@dynamic spacePurpose;

+ (NSString *)parseClassName{
    return @"SpacePurpose";
}

@end
