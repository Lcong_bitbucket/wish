//
//  ZOFeedback.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/12.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "AVObject+Subclass.h"

@interface ZOFeedback : AVObject <AVSubclassing>

@property (nonatomic, copy) NSString *feedback;
@property (nonatomic, copy) NSString *creatorPhone;
@property (nonatomic, copy) NSString *creatorName;
@property (nonatomic, strong) AVUser *createUser;

@end
