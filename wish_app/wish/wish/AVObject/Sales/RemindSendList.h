//
//  RemindSendList.h
//  ZUIIO
//
//  Created by mingdchan on 16/7/11.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface RemindSendList : AVObject<AVSubclassing>

@property (nonatomic, strong) AVUser *remindUser;
@property (nonatomic, copy) NSString *orderId;

@end
