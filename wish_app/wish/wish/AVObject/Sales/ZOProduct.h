//
//  ZOProduct.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "ZOSpace.h"
#import "ZOProductType.h"
#import "ZOProductCategory.h"

#define PRODUCT_KEY_GALLERY @"productGallery"
#define PRODUCT_KEY_Type @"productType"
#define PRODUCT_KEY_Style @"productStyle"
#define PRODUCT_KEY_Comments @"comments"
#define PRODUCT_KEY_Dimension @"productDimension"
#define PRODUCT_KEY_Category @"productCategory"

@interface ZOProduct : AVObject<AVSubclassing>

@property (nonatomic, copy)   NSString *productName;
@property (nonatomic, copy)   NSString *productBrief;
@property (nonatomic, strong) NSNumber *productPrice;
@property (nonatomic, strong) ZOProductType *productType;
@property (nonatomic, strong) NSArray *productStyle;
@property (nonatomic, strong) ZOProductCategory *productCategory;

@property (nonatomic, strong) ZOSpace *space;
@property (nonatomic, copy)   NSString *spaceName;
@property (nonatomic, strong) NSArray *comments;


@property (nonatomic, strong) NSDictionary *waterfallImageSize;
@property (nonatomic, strong) AVFile  *waterfallHeadPic;

@property (nonatomic, strong) AVFile *productCategoryPic;
@property (nonatomic, strong) NSString *productHeadPic;
@property (nonatomic, strong) NSArray *productGallery;
@property (nonatomic, strong) NSArray *productDescribe;
@property (nonatomic, strong) NSArray *productDimension;

@property (nonatomic, assign) int views;
@property (nonatomic, assign) int favoriteTimes;

@property (nonatomic, assign) BOOL isRemoved;//下架
@property (nonatomic, assign) BOOL isWellChosen;//下架

@end
