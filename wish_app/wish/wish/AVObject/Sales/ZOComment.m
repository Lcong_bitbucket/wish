//
//  ZOComment.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/30.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOComment.h"

@implementation ZOComment

@dynamic toUser;
@dynamic commentContent;
@dynamic commentUsername;
@dynamic product;
@dynamic commentUser;
@dynamic comment_rank;
@dynamic fromOrder;

+ (NSString *)parseClassName{
    return @"Comment";
}

@end
