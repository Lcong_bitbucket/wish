//
//  ZOProductStyle.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOProductStyle.h"

@implementation ZOProductStyle
@dynamic productStyle;
@dynamic style;

+ (NSString *)parseClassName{
    return @"ProductStyle";
}

@end
