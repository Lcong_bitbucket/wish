//
//  ZOProductDimension.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZOProductDimension : AVObject<AVSubclassing>

@property (nonatomic, strong) NSString *productDimension;

@property (nonatomic, strong) NSString *productDimensionValue;

@end
