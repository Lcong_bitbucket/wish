//
//  ZOProductStyle.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZOProductStyle : AVObject<AVSubclassing>

@property (nonatomic, assign) int style;
@property (nonatomic, copy  ) NSString *productStyle;

@end
