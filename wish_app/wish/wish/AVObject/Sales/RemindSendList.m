//
//  RemindSendList.m
//  ZUIIO
//
//  Created by mingdchan on 16/7/11.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "RemindSendList.h"

@implementation RemindSendList

@dynamic remindUser;
@dynamic orderId;

+ (NSString *)parseClassName{
    return @"RemindSendList";
}

@end
