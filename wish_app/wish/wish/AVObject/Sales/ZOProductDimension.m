//
//  ZOProductDimension.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOProductDimension.h"

@implementation ZOProductDimension

@dynamic productDimension;
@dynamic productDimensionValue;

+ (NSString *)parseClassName{
    return @"ProductDimension";
}

@end
