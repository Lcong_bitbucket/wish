//
//  ZOProduct.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOProduct.h"

@implementation ZOProduct

@dynamic space;
@dynamic comments;
@dynamic productStyle;
@dynamic waterfallHeadPic;
@dynamic productName;
@dynamic productPrice;
@dynamic productType;

@dynamic productCategory;
@dynamic productCategoryPic;
@dynamic productHeadPic;
@dynamic waterfallImageSize;

@dynamic productDescribe;
@dynamic productGallery;
@dynamic productDimension;
@dynamic views;
@dynamic favoriteTimes;
@dynamic productBrief;
@dynamic spaceName;
@dynamic isRemoved;
@dynamic isWellChosen;

+ (NSString *)parseClassName{
    return @"Product";
}

@end
