//
//  ProductType.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/23.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOProductType.h"

@implementation ZOProductType
@dynamic productType;
@dynamic icon;
@dynamic type;


+ (NSString *)parseClassName{
    return @"ProductType";
}
@end
