//
//  ZOComment.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/30.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "ZOProduct.h"
#import "ZOOrders.h"

@interface ZOComment : AVObject<AVSubclassing>

@property (nonatomic, strong) AVUser *toUser;
@property (nonatomic, copy)   NSString *commentContent;
@property (nonatomic, strong) NSString *commentUsername;
@property (nonatomic, strong) ZOProduct *product;
@property (nonatomic, strong) AVUser *commentUser;
@property (nonatomic, assign) int comment_rank;
@property (nonatomic, strong) ZOOrders *fromOrder;
@property (nonatomic, strong) NSArray *replys;

@end
