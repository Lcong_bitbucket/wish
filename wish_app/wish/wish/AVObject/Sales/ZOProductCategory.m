//
//  ZOProductCategory.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/30.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOProductCategory.h"

@implementation ZOProductCategory

@dynamic productCategory;

+ (NSString *)parseClassName{
    return @"ProductCategory";
}

@end
