//
//  ZOProductCategory.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/30.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZOProductCategory : AVObject<AVSubclassing>

@property (nonatomic, strong) NSString *productCategory;

@end
