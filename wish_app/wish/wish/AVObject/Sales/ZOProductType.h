//
//  ProductType.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/23.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "AVObject+Subclass.h"

@interface ZOProductType : AVObject<AVSubclassing>

@property (nonatomic, assign) int type;
@property (nonatomic, copy  ) NSString *productType;
@property (nonatomic, strong) AVFile *icon;

@end
