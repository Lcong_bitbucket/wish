//
//  ZORefundList.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/28.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZORefundList : AVObject <AVSubclassing>

@property (nonatomic, copy) NSString *rejectReason;
@property (nonatomic, copy) NSString *orderNum;
@property (nonatomic, copy) NSString *chargeNum;
@property (nonatomic, assign) BOOL isVerified;
@property (nonatomic, assign) BOOL isReject;
@property (nonatomic, assign) BOOL isRefundSucceed;
@property (nonatomic, strong) NSDate *refundSucceedDate;
@property (nonatomic, strong) NSDate *verifiedDate;
@end
