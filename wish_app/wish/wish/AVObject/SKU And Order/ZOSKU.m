//
//  ZOSKU.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/10.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOSKU.h"

@implementation ZOSKU

@dynamic productId;
@dynamic product;
@dynamic skuDescribe;
@dynamic price;
@dynamic skuiconUrl;

+ (NSString *)parseClassName{
    return @"SKU";
}

@end
