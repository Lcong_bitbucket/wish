//
//  ZODiscount.m
//  ZUIIO
//
//  Created by mingdchan on 16/10/22.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZODiscount.h"

@implementation ZODiscount

@dynamic isAvailable;
@dynamic discount;
@dynamic discountDescribe;
@dynamic orderLimit;
@dynamic discountBanner;
@dynamic bannerLocalName;
@dynamic showTimes;

+ (NSString *)parseClassName{
    return @"Discount";
}

@end
