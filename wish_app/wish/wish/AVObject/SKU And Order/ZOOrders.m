//
//  ZOOrder.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/10.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOOrders.h"

@implementation ZOOrders

@dynamic orderSkuCode;
@dynamic skuAmountArray;
@dynamic orderUser;
@dynamic spaceID;
@dynamic contactName;
@dynamic contactPhone;
@dynamic expressCompany;
@dynamic expressNum;
@dynamic invoiceTitle;
@dynamic orderRemark;
@dynamic deliveryAddress;
@dynamic isSent;
@dynamic isReceived;
@dynamic isRefund;
@dynamic isPaySucceed;
@dynamic isCanceled;
@dynamic isComplete;
@dynamic isRanked;
@dynamic isDeleted;
@dynamic orderPrice;
@dynamic payDate;
@dynamic chargeID;
@dynamic payChannel;
@dynamic sentDate;
@dynamic receiveDate;
@dynamic discount;
@dynamic discountAmount;

+ (NSString *)parseClassName{
    return @"Orders";
}

@end
