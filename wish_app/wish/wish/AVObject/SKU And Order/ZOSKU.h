//
//  ZOSKU.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/10.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "ZOProduct.h"

@interface ZOSKU : AVObject <AVSubclassing>

@property (nonatomic, copy)   NSString *productId;
@property (nonatomic, strong) ZOProduct *product;
@property (nonatomic, copy)   NSString *skuDescribe;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, copy)   NSString *skuiconUrl;

@end
