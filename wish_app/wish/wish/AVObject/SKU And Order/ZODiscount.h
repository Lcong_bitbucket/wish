//
//  ZODiscount.h
//  ZUIIO
//
//  Created by mingdchan on 16/10/22.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"

@interface ZODiscount : AVObject <AVSubclassing>

@property (nonatomic, assign) BOOL isAvailable;
@property (nonatomic, strong) NSNumber *orderLimit;
@property (nonatomic, strong) NSNumber *discount;
@property (nonatomic, strong) NSString *discountDescribe;
@property (nonatomic, strong) NSString *bannerLocalName;
@property (nonatomic, strong) AVFile *discountBanner;
@property (nonatomic, assign) NSNumber *showTimes;

@end
