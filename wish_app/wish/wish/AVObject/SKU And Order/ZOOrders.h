//
//  ZOOrder.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/10.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "ZOSpace.h"
#import "ZODiscount.h"

typedef NS_ENUM(NSUInteger, RefundStatus) {
    RefundStatusPending,
    RefundStatusSucceeded,
    RefundStatusFailed
};

@interface ZOOrders : AVObject<AVSubclassing>

@property (nonatomic, copy) NSString *orderSkuCode;

@property (nonatomic, strong) NSArray *skuAmountArray;

@property (nonatomic, strong) AVUser *orderUser;

@property (nonatomic, copy) NSString *spaceID;

@property (nonatomic, copy) NSString *contactName;

@property (nonatomic, copy) NSString *contactPhone;

@property (nonatomic, copy) NSString *invoiceTitle;

@property (nonatomic, copy) NSString *orderRemark;

@property (nonatomic, copy) NSString *deliveryAddress;

@property (nonatomic, strong) NSNumber *orderPrice;

@property (nonatomic, assign) BOOL isSent;

@property (nonatomic, assign) BOOL isReceived;

@property (nonatomic, assign) BOOL isRefund;

@property (nonatomic, assign) BOOL isPaySucceed;

@property (nonatomic, assign) BOOL isCanceled;

@property (nonatomic, assign) BOOL isComplete;

@property (nonatomic, assign) BOOL isRanked;

@property (nonatomic, assign) BOOL isDeleted;

@property (nonatomic, strong) NSDate *payDate;

@property (nonatomic, copy) NSString *chargeID;

@property (nonatomic, assign) int payChannel;//0微信,1支付宝

@property (nonatomic, copy) NSString *expressCompany;

@property (nonatomic, copy) NSString *expressNum;

@property (nonatomic, strong) NSDate *sentDate;

@property (nonatomic, strong) NSDate *receiveDate;

@property (nonatomic, strong) ZODiscount *discount;

@property (nonatomic, strong) NSNumber *discountAmount;


//额外的属性
@property (nonatomic, assign) RefundStatus refundStatus;//（三种状态: pending: 处理中; succeeded: 成功; failed: 失败）。

@end


