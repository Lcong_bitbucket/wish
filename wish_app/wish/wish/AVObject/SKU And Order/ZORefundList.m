//
//  ZORefundList.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/28.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZORefundList.h"

@implementation ZORefundList

@dynamic rejectReason;
@dynamic orderNum;
@dynamic chargeNum;
@dynamic isVerified;
@dynamic isReject;
@dynamic isRefundSucceed;
@dynamic refundSucceedDate;
@dynamic verifiedDate;


+ (NSString *)parseClassName{
    return @"RefundList";
}


@end
