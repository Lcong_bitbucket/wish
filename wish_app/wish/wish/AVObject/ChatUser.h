//
//  ChatUser.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/2.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AVUser;

@interface ChatUser : NSObject <CDUserModelDelegate>

@property (nonatomic, strong) NSString *userId;

@property (nonatomic, strong) NSString *username;

@property (nonatomic, strong) NSString *avatarUrl;

- (void)transferFromAVUser:(AVUser*)avUser;

@end
