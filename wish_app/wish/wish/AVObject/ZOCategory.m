//
//  ZOCategory.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOCategory.h"

@implementation ZOCategory

@dynamic spaces;
@dynamic subCategorys;
@dynamic categoryName;
@dynamic hadSubcategory;
@dynamic displayIcon;
@dynamic subCategoryKey;
@dynamic isProduct;
@dynamic products;
@dynamic cbds;
@dynamic isCBD;

+ (NSString *)parseClassName{
    return @"Category";
}

+ (void)getCategorys:(AVArrayResultBlock)block{
    AVQuery *query = [ZOCategory query];
    [query whereKey:kCategoryKeyLevel equalTo:@1];
    [query orderByAscending:kCategoryKeyPriority];
    [query includeKey:kCategoryKeySpaces];
    [query includeKey:kCategoryKeyProducts];
    [query includeKey:kCategoryKeySubCategorys];
    [query includeKey:kCategoryKeyCBDs];
    
    query.cachePolicy = kAVCachePolicyNetworkElseCache;
    //设置缓存有效期
    query.maxCacheAge = 72*3600;
    [query findObjectsInBackgroundWithBlock:block];
}

@end
