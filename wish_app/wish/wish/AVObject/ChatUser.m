//
//  ChatUser.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/2.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ChatUser.h"
#import "AVUser+ZOUser.h"

@implementation ChatUser

- (void)transferFromAVUser:(AVUser *)avUser{
    self.userId = avUser.objectId;
    self.username = avUser.nickname;
    self.avatarUrl = avUser.avatarUrl;
}

@end
