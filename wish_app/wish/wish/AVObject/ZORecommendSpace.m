//
//  ZORecommendSpace.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/3.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZORecommendSpace.h"

@implementation ZORecommendSpace
@dynamic spacePhotos;
@dynamic spaceName;
@dynamic spaceAddress;
@dynamic isHost;
@dynamic isReferee;
@dynamic contactPhone;
@dynamic describe;
@dynamic refereeName;

+ (NSString *)parseClassName{
    return @"RecommendSpace";
}

@end
