//
//  ZOHotSearchStyles.m
//  ZUIIO
//
//  Created by mingdchan on 16/9/8.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOHotSearchStyles.h"

@implementation ZOHotSearchStyles

@dynamic styles;

+ (NSString *)parseClassName{
    return @"HotSearchStyles";
}

@end
