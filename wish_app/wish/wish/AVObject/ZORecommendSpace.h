//
//  ZORecommendSpace.h
//  ZUIIO
//
//  Created by mingdchan on 16/6/3.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "AVObject+Subclass.h"

@interface ZORecommendSpace : AVObject <AVSubclassing>

@property (nonatomic, strong) NSArray *spacePhotos;
@property (nonatomic, strong) NSString *spaceName;
@property (nonatomic, strong) NSString *spaceAddress;
@property (nonatomic, assign) BOOL isHost;
@property (nonatomic, assign) BOOL isReferee;
@property (nonatomic, strong) NSString *contactPhone;
@property (nonatomic, strong) NSString *describe;
@property (nonatomic, strong) NSString *refereeName;

@end
