//
//  AVUser+ZOUser.m
//  ZUIIO
//
//  Created by zuiio on 16/4/27.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVUser+ZOUser.h"

@implementation AVUser (ZOUser)

- (AVFile*)avatar{
    return [self objectForKey:KEY_AVATAR];
}

- (void)setAvatar:(AVFile*)avatar{
    [self setObject:avatar forKey:KEY_AVATAR];
}

- (NSString*)avatarUrl{
    return [self avatar].url;
}

- (NSString*)nickname{
    return [self objectForKey:KEY_NICKNAME];
}

- (NSString*)userBrief{
    return [self objectForKey:KEY_USERBRIEF];
}

- (NSArray *)favoriteSpaces{
    return [self objectForKey:KEY_FAVORITES];
}

- (NSArray *)favoriteProducts{
    return [self objectForKey:KEY_FAVORITES_PRODUCT];
}

- (NSString*)gender{
    return [self objectForKey:KEY_GENDER];
}

- (void)setGender:(NSString*)gender{
    [self setObject:gender forKey:KEY_GENDER];
}

- (void)setNickname:(NSString*)nickname{
    [self setObject:nickname forKey:KEY_NICKNAME];
}

//{
//    "name": "xxx",
//    "phone": "13542656852",
//    "province": "广东省",
//    "city": "深圳市",
//    "area": "南山区",
//    "address": "深圳市南山区海逸阁11G"
//}

- (NSArray *)deliveryAddress{
    return [self objectForKey:KEY_DELIVERY_ADDRESS];
}

- (void)setDeliveryAddress:(NSString*)deliveryAddress{
    [self setObject:deliveryAddress forKey:KEY_DELIVERY_ADDRESS];
}

@end
