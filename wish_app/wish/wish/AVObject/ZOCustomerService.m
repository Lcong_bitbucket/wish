//
//  ZOCustomerService.m
//  ZUIIO
//
//  Created by mingdchan on 16/5/16.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOCustomerService.h"

@implementation ZOCustomerService

@dynamic serviceID;
@dynamic serviceName;

+ (NSString *)parseClassName{
    return @"CustomerService";
}

+ (void)getCustomerServices:(AVArrayResultBlock)block{
    AVQuery *q = [ZOCustomerService query];
    [q findObjectsInBackgroundWithBlock:block];
}

@end
