//
//  ZOSystemMessage.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/6.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOSystemMessage.h"

@implementation ZOSystemMessage

@dynamic messageTitle;
@dynamic messageDetail;
@dynamic messageUrl;
@dynamic messageImage;

+ (NSString *)parseClassName{
    return @"SystemMessage";
}


@end
