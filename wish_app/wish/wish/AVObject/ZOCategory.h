//
//  ZOCategory.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/9.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "AVObject+Subclass.h"

static NSString *const kCategoryKeyLevel = @"level";
static NSString *const kCategoryKeyPriority =@"priority";
static NSString *const kCategoryKeySpaces = @"spaces";
static NSString *const kCategoryKeyProducts = @"products";
static NSString *const kCategoryKeySubCategorys = @"subCategorys";
static NSString *const kCategoryKeyCBDs = @"cbds";

@interface ZOCategory : AVObject <AVSubclassing>

@property (nonatomic, strong) NSString *categoryName;//目录名

@property (nonatomic, assign) BOOL hadSubcategory;//是否有子目录
@property (nonatomic, strong) NSArray *subCategorys;//子目录
@property (nonatomic, strong) NSString *subCategoryKey;//子目录的key
@property (nonatomic, strong) AVFile *displayIcon;//子目录的头图

//非子目录下
@property (nonatomic, assign) BOOL isProduct;//1为商品目录 0 空间目录
@property (nonatomic, assign) BOOL isCBD;
@property (nonatomic, strong) NSArray *spaces;//空间
@property (nonatomic, strong) NSArray *products;//商品
@property (nonatomic, strong) NSArray *cbds;//cbd

@property (nonatomic, assign) BOOL isNearBy;//附近商品
@property (nonatomic, assign) BOOL isRecently;//最近浏览

@property (nonatomic, assign) CGFloat horizontalOffset;

+ (void)getCategorys:(AVArrayResultBlock)block;

@end
