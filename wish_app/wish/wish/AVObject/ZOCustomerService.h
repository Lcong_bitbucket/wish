//
//  ZOCustomerService.h
//  ZUIIO
//
//  Created by mingdchan on 16/5/16.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVObject.h"
#import "AVObject+Subclass.h"

@interface ZOCustomerService : AVObject <AVSubclassing>

@property (nonatomic, copy) NSString *serviceID;
@property (nonatomic, copy) NSString *serviceName;

+ (void)getCustomerServices:(AVArrayResultBlock)block;
    
@end
