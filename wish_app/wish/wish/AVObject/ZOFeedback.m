//
//  ZOFeedback.m
//  ZUIIO
//
//  Created by mingdchan on 16/6/12.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOFeedback.h"

@implementation ZOFeedback

@dynamic feedback;
@dynamic creatorPhone;
@dynamic creatorName;
@dynamic createUser;

+ (NSString *)parseClassName{
    return @"Feedback";
}

@end
