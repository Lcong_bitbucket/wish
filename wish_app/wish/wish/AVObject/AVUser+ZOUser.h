//
//  AVUser+ZOUser.h
//  ZUIIO
//
//  Created by zuiio on 16/4/27.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "AVUser.h"

#define KEY_USERNAME @"username"
#define KEY_AVATAR @"avatar"
#define KEY_NICKNAME @"nickname"
#define KEY_GENDER @"gender"
#define KEY_USERBRIEF @"userBrief"
#define KEY_FAVORITES @"favoriteSpaces"
#define KEY_FAVORITES_PRODUCT @"favoriteProducts"
#define KEY_DELIVERY_ADDRESS @"deliveryAddress"

@interface AVUser (ZOUser)

- (AVFile*)avatar;

- (void)setAvatar:(AVFile*)avatar;

- (NSString*)avatarUrl;

- (NSString*)nickname;

- (NSString*)userBrief;

- (NSString*)gender;

- (void)setGender:(NSString*)gender;

- (NSArray *)favoriteSpaces;

- (NSArray *)favoriteProducts;

- (void)setNickname:(NSString*)nickname;

- (NSArray *)deliveryAddress;

- (void)setDeliveryAddress:(NSString*)deliveryAddress;

@end
