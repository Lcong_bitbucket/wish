//
//  XCBaseViewController.h
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCBaseViewController : UIViewController
{
    
}
@property (nonatomic,assign) BOOL hiddenNavBarWhenPush;//隐藏导航栏
@property (nonatomic,assign) BOOL clearNavBar;//透明导航栏
@property (nonatomic,strong) UIColor *navColor;//设置导航栏颜色
- (void)customShareButton;
- (void)customBackButton;
- (void)customNavigationBarItemWithImageName:(NSString*)imgName title:(NSString *)title isLeft:(bool)isLeft;
- (void)rightBarButtonAction;
- (void)leftBarButtonAction;
- (void)goBack;

@end
