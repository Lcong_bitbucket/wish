//
//  XCWKWebViewController.m
//  xcwl
//
//  Created by 聪 on 16/7/14.
//  Copyright © 2016年 聪. All rights reserved.
//


#import "XCWKWebViewController.h"

#import "TUSafariActivity.h"
#import "ARChromeActivity.h"

static void *KINWebBrowserContext = &KINWebBrowserContext;

@interface XCWKWebViewController () <UIAlertViewDelegate,WKNavigationDelegate,UIScrollViewDelegate>
{
    NSMutableArray *imageArray;
}

@end

@implementation XCWKWebViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isShowIMG = NO;
    self.isShowHtmlTitle = YES;
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.allowsInlineMediaPlayback = YES;
    config.mediaPlaybackRequiresUserAction = NO;
    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    
    [self initNavBar];
    
    if(self.webView) {
        [self.webView setFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64)];
        [self.webView setNavigationDelegate:self];
        [self.webView setUIDelegate:self];
        [self.webView setMultipleTouchEnabled:YES];
        self.webView.scrollView.delegate = self;
        [self.webView.scrollView setAlwaysBounceVertical:YES];
        [self.view addSubview:self.webView];
        
             
        [self.webView addObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress)) options:0 context:KINWebBrowserContext];
    }
 
    if (self.bridge) { return;}
    [WebViewJavascriptBridge enableLogging];
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:self.webView];
    [self.bridge setWebViewDelegate:self];
    
    [self.bridge registerHandler:@"handleClick" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSLog(@"response: %@", data);
        goADPage(data, self);
        responseCallback(@"成功");
    }];
    
//    [_bridge registerHandler:@"testObjcCallback" handler:^(id data, WVJBResponseCallback responseCallback) {
//        NSLog(@"调用: %@", data);
//        responseCallback(@"Response from testObjcCallback");
//    }];
    
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [self.progressView setTrackTintColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [self.progressView setFrame:CGRectMake(0, 0, Screen_Width, self.progressView.frame.size.height)];
//    [self.progressView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    [self.webView addSubview:self.progressView];

}

- (void)initNavBar{
    [self customNavigationBarItemWithImageName:@"back-arrow" title:nil isLeft:YES];
    
//    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
//    
//    UIButton* backButton= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
//    [backButton setImage:[UIImage imageNamed:@"back-arrow"] forState:UIControlStateNormal];
//    [backButton setBackgroundColor:[UIColor clearColor]];
//    [backButton addTarget:self action:@selector(webBack) forControlEvents:UIControlEventTouchUpInside];
//    [leftView addSubview:backButton];
//    
//    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(40, 0, 40, 44)];
//    [closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
//    [closeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 5)];
//    closeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
//    [closeBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//    [closeBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
//    [leftView addSubview:closeBtn];
//    
//    //创建导航栏按钮UIBarButtonItem
//    UIBarButtonItem* backItem= [[UIBarButtonItem alloc] initWithCustomView:leftView];
//    
//    UIBarButtonItem *negativeSpacer1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//    negativeSpacer1.width = -15;//这个数值可以根据情况自由变化
//    
//    self.navigationItem.leftBarButtonItems = @[negativeSpacer1, backItem];
}

- (void)webBack{
    if([self.webView canGoBack]){
        [self.webView goBack];
    } else {
        [self goBack];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
 
}

#pragma mark - Public Interface

- (void)loadRequest:(NSURLRequest *)request {
    if(self.webView) {
        [self.webView loadRequest:request];
    }
}

- (void)loadURL:(NSURL *)URL {
    [self loadRequest:[NSURLRequest requestWithURL:URL]];
}

- (void)loadURLString:(NSString *)URLString {
    NSURL *URL = [NSURL URLWithString:URLString];
    [self loadURL:URL];
}

- (void)loadHTMLString:(NSString *)HTMLString {
    if(self.webView) {
        [self.webView loadHTMLString:HTMLString baseURL:nil];
    }
}





#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
  
//    [UIView animateWithDuration:.8 animations:^{
      //    }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
 
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
 
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
  
    XCLog(@"start load web");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if(webView == self.webView) {
        if(self.isShowIMG)
            imageArray = setDefaultJSEvent(webView);
    }
    if(self.isShowHtmlTitle){
        [webView evaluateJavaScript:@"document.title" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            self.title = result;
        }];
    }
   
    XCLog(@"finish load web");

}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
      withError:(NSError *)error {
    if(webView == self.webView) {
        [webView loadHTMLString:@"加载内容失败,请重试..." baseURL:nil];
    }
    XCLog(@"fail load web");
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation
      withError:(NSError *)error {
  
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
   
        NSURL *URL = navigationAction.request.URL;
        NSString *requestString = [[URL absoluteString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        XCLog(@"requestring = %@",requestString);

        if(![self checkCustomUrl:requestString]){
            self.NotFirstLoad = YES;
            
            if ([URL.scheme isEqualToString:@"xcshowimg"]) {
                NSString* path = [URL.absoluteString substringFromIndex:[@"xcshowimg:" length]];
                path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSInteger index = 0;
                for(int i = 0; i < imageArray.count;i++){
                    if([path isEqualToString:imageArray[i]]){
                        index = i;
                        break;
                    }
                }
                openWithMJPhoto(index, imageArray, nil);
                 decisionHandler(WKNavigationActionPolicyCancel);

            } else {
                    if(!navigationAction.targetFrame) {
                        [self loadURL:URL];
                        decisionHandler(WKNavigationActionPolicyCancel);
                    } else {
                        decisionHandler(WKNavigationActionPolicyAllow);
                    }
            }

        } else  {
            decisionHandler(WKNavigationActionPolicyCancel);
        }
}

- (BOOL)checkCustomUrl:(NSString *)urlString {
    return NO;
}

#pragma mark - WKUIDelegate

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}


#pragma mark - Estimated Progress KVO (WKWebView)

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))] && object == self.webView) {
        [self.progressView setAlpha:1.0f];
        BOOL animated = self.webView.estimatedProgress > self.progressView.progress;
        [self.progressView setProgress:self.webView.estimatedProgress animated:animated];
        XCLog(@"progress = %lf",self.webView.estimatedProgress);
        // Once complete, fade out UIProgressView
        if(self.webView.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self.progressView setProgress:0.0f animated:NO];
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - Dealloc

- (void)dealloc {
    self.webView.scrollView.delegate = nil;
    [self.webView setNavigationDelegate:nil];
    [self.webView setUIDelegate:nil];
    if ([self isViewLoaded]) {
        [self.webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    }
}


@end

