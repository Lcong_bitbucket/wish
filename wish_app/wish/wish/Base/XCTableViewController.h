//
//  XCTableViewController.h
//  VKTemplateForiPhone
//
//  Created by 聪 on 16/4/5.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "XCBaseViewController.h"

@interface XCTableViewController : XCBaseViewController
@property (nonatomic,strong) UITableView *superTableView;
@property (nonatomic,assign) NSInteger curPage;
@property (nonatomic,assign) BOOL scrollHiddenNav;//滚动隐藏nav
@property (nonatomic,assign) BOOL isHiddenNav;

- (void)defaultConfig:(UITableView *)tableView; //默认配置
- (void)openMJRefreshHeader:(BOOL)header Footer:(BOOL)footer tableView:(UITableView *)tableView;
- (void)stopRefresh:(BOOL)isLast;//结束MJRfresh 方式
- (void)beginRefreshing;//开始头部刷新
- (void)hiddenTop;
- (void)refresh;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
@end
