//
//  XCBaseViewController.m
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "UINavigationBar+Awesome.h"

@interface XCBaseViewController ()

@end

@implementation XCBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id) self;
    self.navColor = UIColorFromRGB(0xffffff);
    [self titleColor:[UIColor blackColor]];
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    // Do any additional setup after loading the view.
}

- (void)titleColor:(UIColor *)color{

    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeTextColor: color,
                                                                    UITextAttributeFont : [UIFont boldSystemFontOfSize:18]};
}
 
- (void)viewWillAppear:(BOOL)animated{
    if (self.hiddenNavBarWhenPush){
        [self.navigationController setNavigationBarHidden:YES animated:animated];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }else{
        [self.navigationController setNavigationBarHidden:NO animated:animated];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.barTintColor = self.navColor;
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBackgroundImage:[UIImage imageWithColor:self.navColor] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault]; //此处使底部线条颜色为红色
    [navigationBar setShadowImage:[UIImage imageWithColor:self.navColor]];

    if(self.clearNavBar){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        self.navigationController.navigationBar.translucent = YES;
        self.edgesForExtendedLayout=UIRectEdgeTop;
        [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0]];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    }
  
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(self.clearNavBar){
        self.edgesForExtendedLayout=UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar lt_reset];
    }
    
}

- (void)customBackButton {
    [self customNavigationBarItemWithImageName:@"back-arrow" title:nil isLeft:YES];
}
- (void)customShareButton{
    [self customNavigationBarItemWithImageName:@"icon_share" title:nil isLeft:NO];
}
//自定义样式的导航栏item-- 用图片
- (void)customNavigationBarItemWithImageName:(NSString*)imgName title:(NSString *)title isLeft:(bool)isLeft {
    
    CGFloat btnW = 0;
    CGFloat btnH = 0;
    UIImage *im = [UIImage imageNamed:imgName];
    if (imgName){
        btnW = im.size.width;
        btnH = im.size.height;
        btnW = btnW > 44 ? btnW :44;
        btnH = btnH > 44 ? btnH :44;
    }
    if(title){
        CGSize titleSize = GetStringSize(title, 15, nil, CGSizeMake(100, 30));
        btnW = titleSize.width;
        btnH = 40;
    }
    
    UIButton* jumpButton= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnW, btnH)];//微调
    [jumpButton setTitle:title forState:UIControlStateNormal];
    [jumpButton.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    [jumpButton setTitleColor:UIColorFromRGB(0x7e8597) forState:UIControlStateNormal];
    [jumpButton.titleLabel setTextAlignment:2];
    [jumpButton setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    if (!isLeft) {
        [jumpButton addTarget:self action:@selector(rightBarButtonAction) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [jumpButton addTarget:self action:@selector(leftBarButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    UIBarButtonItem* actionItem= [[UIBarButtonItem alloc] initWithCustomView:jumpButton];
    if (isLeft) {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -15;//这个数值可以根据情况自由变化
         self.navigationItem.leftBarButtonItems = @[negativeSpacer, actionItem];
    }
    else {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = 0;//这个数值可以根据情况自由变化
        
        self.navigationItem.rightBarButtonItems = @[actionItem];
    }
}

//导航栏右键响应函数，重写此函数，响应点击事件
- (void)rightBarButtonAction {
    XCLog(@"need to implement this methor");
}

- (void)leftBarButtonAction{
    [self goBack];
}

//返回键响应函数，重写此函数，实现返回前执行一系列操作
- (void)goBack {
    
    if([self.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]){
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
