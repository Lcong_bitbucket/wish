//
//  DynamicHeadTableViewCell.h
//  wish
//
//  Created by Lcong on 2017/3/18.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@interface DynamicHeadTableViewCell : UITableViewCell<SDCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet SDCycleScrollView *cycleScroll;


@property (nonatomic,strong) NSMutableArray *array;
@end
