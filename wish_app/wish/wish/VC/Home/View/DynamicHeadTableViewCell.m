//
//  DynamicHeadTableViewCell.m
//  wish
//
//  Created by Lcong on 2017/3/18.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "DynamicHeadTableViewCell.h"

@implementation DynamicHeadTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setArray:(NSMutableArray *)array{
    _array = array;
//    _cycleScroll = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero
//                                                            delegate:self
//                                                    placeholderImage:[UIImage imageNamed:@"tabbar_icon0_normal"]];
    NSMutableArray *pics = [[NSMutableArray alloc] init];
    NSMutableArray *titles = [NSMutableArray new];
    
    for(NSInteger i= 0; i < _array.count ; i ++){
        [titles addObject:[NSString stringWithFormat:@"%d/%d",i+1,_array.count]];
        [pics addObject:_array[i]];
    }
    _cycleScroll.localizationImageNamesGroup = pics;
    _cycleScroll.titlesGroup = titles;
    _cycleScroll.autoScroll = NO;
    _cycleScroll.showPageControl = NO;
//    _cycleScroll.ti
    _cycleScroll.titleLabelBackgroundColor = [UIColor clearColor];// 图片对应的标题的 背景色。（因为没有设标题）
    _cycleScroll.titleLabelTextColor = [UIColor whiteColor];
  [SDCycleScrollView clearImagesCache];// 清除缓存。
}

@end
