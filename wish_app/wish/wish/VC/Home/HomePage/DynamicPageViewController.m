//
//  DynamicPageViewController.m
//  wish
//
//  Created by RSTaylor on 17/4/9.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "DynamicPageViewController.h"
#import "HomePageController.h"
#import "SearchBarView.h"

@interface DynamicPageViewController ()
{
    SearchBarView *_searchBar;

}
@end

@implementation DynamicPageViewController

- (NSArray<NSString *> *)titles {
    return @[
             @"关注",
             @"附近",
             @"穿搭",
             @"饰品",
             @"居家",
             @"运动",
             ];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _searchBar = [[[NSBundle mainBundle] loadNibNamed:@"SearchBarView" owner:self options:nil] lastObject];
    
    self.navigationItem.titleView = _searchBar;

        // Do any additional setup after loading the view from its nib.
}

#pragma mark - WMPageController DataSource
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.titles[index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
 
    return [[HomePageController alloc] init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
