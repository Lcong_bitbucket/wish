//
//  DynamicDetailViewController.m
//  wish
//
//  Created by Lcong on 2017/3/16.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "DynamicDetailViewController.h"
#import "DynamicHeadTableViewCell.h"
#import "CommentTableViewCell.h"
#import "UIButton+ImageTitleSpacing.h"

@interface DynamicDetailViewController ()
@property (nonatomic,strong) NSMutableArray *picArray;
@property (nonatomic,strong) NSMutableArray *commentArray;

@end

@implementation DynamicDetailViewController

- (void)viewDidLoad {
    self.clearNavBar = YES;
    [super viewDidLoad];
    
    [self defaultConfig:self.tableView];
    _picArray = [NSMutableArray new];
    [_picArray addObjectsFromArray:@[@"111",
                                     @"222.png",
                                     @"333.png"]];
    _commentArray = [NSMutableArray new];
    
    [_commentArray addObjectsFromArray:@[@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@""]];
    
    [self customNavigationBarItemWithImageName:@"backk" title:nil isLeft:YES];

    [self customNavigationBarItemWithImageName:@"圆" title:nil isLeft:NO];
    
    [_zanBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:5];
     [_commentBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:5];
     [_guanzhuBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:5];
    // Do any additional setup after loading the view from its nib.
}
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 1;
    } else if(section == 1){
        return _commentArray.count;
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        NSString *identifier = @"DynamicHeadTableViewCell";
        DynamicHeadTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        
        cell.array = _picArray;
        
        return cell;
    } else if(indexPath.section == 1){
        NSString *identifier = @"CommentTableViewCell";
        CommentTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
         return cell;
    }
    return nil;
  
}

#pragma mark UITableViewDelegate 列表点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
