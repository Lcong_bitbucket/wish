//
//  RSCell.m
//  wish
//
//  Created by RSTaylor on 17/2/21.
//  Copyright © 2017年 RSTaylor. All rights reserved.
//

#import "RSCell.h"

@implementation RSCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(RSModel *)model{

    _model = model;
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:@"loading"]];
    
    

}

@end
