//
//  RSModel.h
//  wish
//
//  Created by RSTaylor on 17/2/21.
//  Copyright © 2017年 RSTaylor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSModel : NSObject

@property (nonatomic, strong) NSNumber *h; // 高度
@property (nonatomic, strong) NSNumber *w; // 宽度
@property (nonatomic, copy) NSString *img; // 图片urlString
@property (nonatomic, copy) NSString *userIcon; // 用户头像

@property(nonatomic,strong) NSString *userName;//用户名称
@property(nonatomic,assign) BOOL isSelect;//是否点赞
@property(nonatomic,strong) NSString *NumberString;//用户数
@property(nonatomic,strong) NSString *textLabelString;



@end
