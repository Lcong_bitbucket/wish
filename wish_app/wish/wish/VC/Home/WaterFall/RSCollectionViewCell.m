//
//  RSCollectionViewCell.m
//  wish
//
//  Created by RSTaylor on 17/2/23.
//  Copyright © 2017年 RSTaylor. All rights reserved.
//

#import "RSCollectionViewCell.h"

@implementation RSCollectionViewCell


-(void)setModel:(RSModel *)model{

    _model = model;
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:@"loading"]];
    
//        [_imageView sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:@"loading"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        _imageView.contentMode = UIViewContentModeScaleAspectFill;
//    }];//防止placeholder图片被拉伸变形
//
    
    
    
//    _TextLabel.text = _model.textLabelString;
//    
//     [_userIcon sd_setImageWithURL:[NSURL URLWithString:model.userIcon] placeholderImage:[UIImage imageNamed:@"loading"]];
//    _userNameLabel.text = _model.userName;
//    _numberLabel.text = _model.NumberString;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

@end
