//
//  RSWaterFallLayout.h
//  wish
//
//  Created by RSTaylor on 17/2/21.
//  Copyright © 2017年 RSTaylor. All rights reserved.
//

#import <UIKit/UIKit.h>


@class RSWaterFallLayout;

@protocol RSWaterFallLayoutDelegate <NSObject>

@required
// 返回index位置下的item的高度
- (CGFloat)waterFallLayout:(RSWaterFallLayout *)waterFallLayout heightForItemAtIndex:(NSUInteger)index width:(CGFloat)width;

@optional
// 返回瀑布流显示的列数
- (NSUInteger)columnCountOfWaterFallLayout:(RSWaterFallLayout *)waterFallLayout;
// 返回行间距
- (CGFloat)rowMarginOfWaterFallLayout:(RSWaterFallLayout *)waterFallLayout;
// 返回列间距
- (CGFloat)columnMarginOfWaterFallLayout:(RSWaterFallLayout *)waterFallLayout;
// 返回边缘间距
- (UIEdgeInsets)edgeInsetsOfWaterFallLayout:(RSWaterFallLayout *)waterFallLayout;

@end

@interface RSWaterFallLayout : UICollectionViewLayout


/** 代理 */
@property (nonatomic, weak) id<RSWaterFallLayoutDelegate> delegate;



@end
