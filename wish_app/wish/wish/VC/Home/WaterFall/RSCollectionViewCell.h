//
//  RSCollectionViewCell.h
//  wish
//
//  Created by RSTaylor on 17/2/23.
//  Copyright © 2017年 RSTaylor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSModel.h"

@interface RSCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *TextLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userIcon;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *shoucangBtn;
@property (strong, nonatomic) IBOutlet UILabel *numberLabel;

@property(nonatomic,strong) RSModel *model;

@end
