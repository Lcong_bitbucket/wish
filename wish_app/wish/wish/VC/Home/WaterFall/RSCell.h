//
//  RSCell.h
//  wish
//
//  Created by RSTaylor on 17/2/21.
//  Copyright © 2017年 RSTaylor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSModel.h"

@interface RSCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *TextLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userIcon;//用户icon
@property (strong, nonatomic) IBOutlet UILabel *userNameLab;//用户名
@property (strong, nonatomic) IBOutlet UILabel *numberLab;//收藏人数


@property(nonatomic,strong) RSModel *model;

@end
