//
//  UserManager.m
//  VKTemplateForiPhone
//
//  Created by licong on 16/3/30.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "UserManager.h"
static UserManager *sharedInstance = nil;
@implementation UserManager
+ (instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)getCurUserModelFinish:(void (^)(UserModel *userModel))finish{
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    NSInteger user_id = [defaults integerForKey:@"user_id"];
    [self getUserModels:@[@{@"user_id":@(user_id)}] finish:^(NSMutableArray *userModels) {
        UserModel *userModel = [userModels lastObject];
        if(userModel){
            if(finish){
                finish(userModel);
            }
        }
    }];
}

- (BOOL)saveCurUserModel:(UserModel *)model{
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    if([self saveUserModel:model]){
        [defaults setInteger:model.user_id forKey:@"user_id"];
        return YES;
    } else {
        return NO;
    }
}



- (BOOL)saveUserModel:(UserModel *)model{
   UserModel *u = [[UserModel alloc] init];
    u = model;
    if(u){
        if([u isExistsFromDB]){
            if([u updateToDB]){
                return YES;
            } else {
                return NO;
            }
        } else{
            if([u saveToDB]){
                return YES;
            } else {
                return NO;
            }
        }
        
    } else{
        return NO;
        
    }
}

- (void)getUserModels:(NSArray *)param finish:(void (^)(NSMutableArray *userModels))finish{
    if([param count] >0){
        NSString *key = [[[param lastObject] allKeys] lastObject];
        NSMutableArray *values = [[NSMutableArray alloc] init];
        for(NSDictionary *d in param){
            [values addObject:[[d allValues] lastObject]];
        }
//        NSMutableArray *userModels = [UserModel searchWithWhere:@{key:values} orderBy:nil offset:0 count:0];
//        
//        if(userModels){
//            if(finish){
//                finish(userModels);
//            }
//        }
//        XCLog(@"查询数据1 %@",userModels);
        if([key isEqualToString:@"user_code"] ){
            NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
            [params setValue:[values componentsJoinedByString:@","] forKey:@"user_codes"];
            [params setValue:[[UserSessionCenter shareSession] getSessionId] forKey:@"sessionid"];
//            [params setValue:app_delegate().sessionId forKey:@"user_id"];
            XCBaseCommand *command = [XCBaseCommand new];
            command.api = @"appUser_getUserInfosListByCode.action";
            command.params = params;
            command.cachePolicy = XCHTTPClientReturnCacheDataElseLoad;

            [command setResponseBlock:^(NSDictionary *responseObject, NSError *error) {
                if(error){
                    
                } else {
                    if([responseObject[@"status"] integerValue] == 1){
                        
                        NSMutableArray *users = [UserModel mj_objectArrayWithKeyValuesArray:responseObject[@"rows"]];
//                        for(UserModel *u in users){
//                            [self saveUserModel:u];
//                        }
//                        
//                        NSMutableArray *userArray = [UserModel searchWithWhere:@{key:values} orderBy:nil offset:0 count:0];
//                        if(users){
                            if(finish){
                                finish(users);
                            }
                            //                        XCLog(@"查询数据2 %@",userArray);
//                        }
                    } else{
                        //                    XCLog(@"请求user_codes错误");
                    }
                }
            }];
            [[XCHttpClient sharedInstance] request:command];
            
            
//            Request_POST(AFSERVER_DATA, @"appUser_getUserInfosListByCode.action", params, NO, self, ^(id responseObject) {
//                if([responseObject[@"status"] integerValue] == 1){
//                    
//                    NSMutableArray *users = [UserModel mj_objectArrayWithKeyValuesArray:responseObject[@"rows"]];
//                    for(UserModel *u in users){
//                        [self saveUserModel:u];
//                    }
//                    
//                    NSMutableArray *userArray = [UserModel searchWithWhere:@{key:values} orderBy:nil offset:0 count:0];
//                    if(userArray){
//                        if(finish){
//                            finish(userArray);
//                        }
////                        XCLog(@"查询数据2 %@",userArray);
//                    }
//                } else{
////                    XCLog(@"请求user_codes错误");
//                }
//            }, ^(NSError *error) {
////                XCLog(@"请求user_codes失败")
//            });
        }
    } else{
        XCLog(@"key值为空");
    }
}
@end
