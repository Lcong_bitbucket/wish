//
//  UserModel.h
//  VKTemplateForiPhone
//
//  Created by licong on 16/3/7.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "BaseModel.h"

@interface UserModel : BaseModel

@property (nonatomic,assign) NSInteger user_id; //用户ID
@property (nonatomic,strong) NSString *contact_address;//地区
@property (nonatomic,strong) NSString *check_reason;//原因
@property (nonatomic,assign) NSInteger company_relate_status; //审核结果 0 未审核    1 未通过   2 已通过认证
@property (nonatomic,strong) NSString *photo; //图像
@property (nonatomic,assign) NSInteger follow_id; //有字段并且大于0表示已关注
@property (nonatomic,strong) NSString *user_nick;//昵称
@property (nonatomic,strong) NSString *friend_remark;//好友备注
@property (nonatomic,assign) NSInteger user_type; //用户类型 //
@property (nonatomic,assign) NSInteger aim_count; //粉丝数

@property (nonatomic,strong) NSString *business_card_positive;//名片正面
@property (nonatomic,strong) NSString *business_card_osite;//名片反面
@property (nonatomic,strong) NSString *user_phone;//电话
@property (nonatomic,strong) NSString *address_detail;//地址详细
@property (nonatomic,assign) NSInteger company_id; //公司ID
@property (nonatomic,strong) NSString *company_name;//公司名称
@property (nonatomic,strong) NSString *user_name;//真实姓名
@property (nonatomic,strong) NSString *company_position;//职位
@property (nonatomic,strong) NSString *emails;//邮箱
@property (nonatomic,assign) NSInteger invent_num; //访问量
@property (nonatomic,assign) NSInteger is_live; //是否直播
@property (nonatomic,assign) NSInteger conversation_counts; //咨询量
@property (nonatomic,assign) NSInteger cost; //材币
@property (nonatomic,assign) NSInteger sex; //性别  （1男  2 女）
@property (nonatomic,assign) NSInteger is_recommend; //是否推荐（0未推荐   1 已推荐）
@property (nonatomic,assign) NSInteger product_counts; //产品数
@property (nonatomic,assign) NSInteger good_comments; //好评率
@property (nonatomic,assign) NSInteger col_id; //有字段并且大于0表示已收藏

//环信
@property (nonatomic,strong) NSString *user_code;
@end
