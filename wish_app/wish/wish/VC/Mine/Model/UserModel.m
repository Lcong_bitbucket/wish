//
//  UserModel.m
//  VKTemplateForiPhone
//
//  Created by licong on 16/3/7.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel
+(NSString *)getPrimaryKey{
    return @"user_id";
}

- (void)setUser_code:(NSString *)user_code{
    _user_code = [user_code lowercaseString];
}

- (NSString *)user_nick{
    if(_friend_remark.length > 0){
        return _friend_remark;
    } else
        return _user_nick;
}
@end
