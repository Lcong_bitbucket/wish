//
//  UserManager.h
//  VKTemplateForiPhone
//
//  Created by licong on 16/3/30.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
@interface UserManager : NSObject
+ (instancetype)sharedInstance;

- (void)getCurUserModelFinish:(void (^)(UserModel *userModel))finish;
- (BOOL)saveCurUserModel:(UserModel *)model;

//- (void)getUserModel:(NSDictionary *)param finish:(void (^)(UserModel *userModel))finish;
- (BOOL)saveUserModel:(UserModel *)model;

- (void)getUserModels:(NSArray *)param finish:(void (^)(NSMutableArray *userModels))finish;
@end
