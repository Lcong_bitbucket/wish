//
//  PersonViewController.m
//  wish
//
//  Created by Lcong on 2017/3/26.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "PersonViewController.h"
#import "WMPanGestureRecognizer.h"
#import "MyDynamicController.h"
#import "MarkViewController.h"
#import "PersonHeaderView.h"

static CGFloat const kWMHeaderViewHeight = 300;
static CGFloat const kNavigationBarHeight = 0;


@interface PersonViewController (){
    PersonHeaderView *_header;
}

@property (nonatomic, assign) CGFloat viewTop;

@property (nonatomic, strong) NSArray *musicCategories;
@property (nonatomic, strong) WMPanGestureRecognizer *panGesture;
@property (nonatomic, assign) CGPoint lastPoint;

@end

@implementation PersonViewController
- (NSArray *)musicCategories {
    if (!_musicCategories) {
        _musicCategories = @[@"动态", @"标签"];
    }
    return _musicCategories;
}

- (instancetype)init {
    if (self = [super init]) {
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 15;
        self.menuViewStyle = WMMenuViewStyleLine;
        self.menuItemWidth = [UIScreen mainScreen].bounds.size.width / self.musicCategories.count;
        self.menuHeight = 45;
        self.viewTop = kWMHeaderViewHeight;
        self.titleColorSelected = UIColorFromRGB(0xff0000);
        self.titleColorNormal = UIColorFromRGB(0x999999);
    }
    return self;
}

- (void)viewDidLoad {
    self.hiddenNavBarWhenPush = YES;
    [super viewDidLoad];
    
    self.panGesture = [[WMPanGestureRecognizer alloc] initWithTarget:self action:@selector(panOnView:)];
    [self.view addGestureRecognizer:self.panGesture];
    
    _header = [[[NSBundle mainBundle] loadNibNamed:@"PersonHeaderView" owner:self options:nil] lastObject];
    _header.frame = CGRectMake(0, 0, self.view.width, 300);
     [self.view addSubview:_header];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _header.frame = CGRectMake(0, 0, Screen_Width, 300);

}


- (void)panOnView:(WMPanGestureRecognizer *)recognizer {
    NSLog(@"pannnnnning received..");
    
    CGPoint currentPoint = [recognizer locationInView:self.view];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.lastPoint = currentPoint;
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint velocity = [recognizer velocityInView:self.view];
        CGFloat targetPoint = velocity.y < 0 ? kNavigationBarHeight : kNavigationBarHeight + kWMHeaderViewHeight;
        NSTimeInterval duration = fabs((targetPoint - self.viewTop) / velocity.y);
        
        if (fabs(velocity.y) * 1.0 > fabs(targetPoint - self.viewTop)) {
            NSLog(@"velocity: %lf", velocity.y);
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                self.viewTop = targetPoint;
            } completion:nil];
            
            return;
        }
    }
    CGFloat yChange = currentPoint.y - self.lastPoint.y;
    self.viewTop += yChange;
    self.lastPoint = currentPoint;
}

// MARK: ChangeViewFrame (Animatable)
- (void)setViewTop:(CGFloat)viewTop {
    
    _viewTop = viewTop;
    
    if (_viewTop <= kNavigationBarHeight) {
        _viewTop = kNavigationBarHeight;
    }
    
    if (_viewTop > kWMHeaderViewHeight + kNavigationBarHeight) {
        _viewTop = kWMHeaderViewHeight + kNavigationBarHeight;
    }
    
    self.viewFrame = CGRectMake(0, _viewTop, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - _viewTop);
    
    _header.frame = CGRectMake(0, _viewTop - kWMHeaderViewHeight, Screen_Width, kWMHeaderViewHeight);
}

#pragma mark - Datasource & Delegate
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.musicCategories.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if(index == 0){
        MyDynamicController *vc = [[MyDynamicController alloc] init];
        vc.hiddenNavBarWhenPush = YES;
        return vc;
    } else if(index == 1){
        MarkViewController *vc = [[MarkViewController alloc] init];
        vc.hiddenNavBarWhenPush = YES;
        return vc;
    }
    return nil;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.musicCategories[index];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
