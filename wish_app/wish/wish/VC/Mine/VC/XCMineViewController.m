//
//  XCMineViewController.m
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMineViewController.h"
#import "XCMineListTableViewCell.h"
#import "XCMineListModel.h"
#import "HFStretchableTableHeaderView.h"
#import "XCMineListHeaderView.h"
#import "XCFeedBackViewController.h"
#import "LoginViewController.h"
#import "PersonViewController.h"

@interface XCMineViewController () {
    XCMineListHeaderView *header;
    NSString *tel;
    BOOL _isLogin;
}
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic, strong) HFStretchableTableHeaderView* stretchableTableHeaderView;

@end

@implementation XCMineViewController

- (void)viewDidLoad {
    self.hiddenNavBarWhenPush = YES;
    [super viewDidLoad];
    self.dataArray = [NSMutableArray new];
    [self getMindList];
    
    [self defaultConfig:self.tableView];
    header = [[[NSBundle mainBundle] loadNibNamed:@"XCMineListHeaderView" owner:self options:nil] lastObject];
    
    WEAKSELF;
    [header setDidClickBlock:^(NSInteger type) {
        if(type == 0 || type == 1){
            [weakSelf goMineInfo:type];
        } else if(type == 2){
            
         }
    }];
    // Do any additional setup after loading the view from its nib.
    _stretchableTableHeaderView = [HFStretchableTableHeaderView new];
    [_stretchableTableHeaderView stretchHeaderForTableView:self.tableView withView:header];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestInfo];
}

- (void)requestInfo{
    
    if(!_isLogin){
        header.label2.text = @"立即登录";
        header.label1.text = @"";
        header.label3.text = @"";
        [header.avatar setImageURLStr:ImgUrl(@"") placeholder:Default_Avatar_Image_1];
    } else {
        header.label2.text = @"动态：45";
        header.label1.text = @"王尼玛";
        header.label1.text = @"心愿单：145";
        header.avatar.image = [UIImage imageNamed:@"avatar"];
     }
    
 
}


- (void)refreshUI{
//    XCUserManager *manager =[XCUserManager sharedInstance];
//    header.model = manager.userModel;
//    for(int i = 0; i < _dataArray.count ;i ++){
//        NSArray *tempArray = _dataArray[i];
//        for(XCMineListModel *model in tempArray){
//            if([model.title isEqualToString:@"退出"]){
//                if([[XCUserManager sharedInstance] isLogin]){
//                    model.isHidden = 0;
//                } else {
//                    model.isHidden = 1;
//                }
//            }
//            else if([model.title isEqualToString:@"联系客服"]){
//                model.content = tel;
//            }else if([model.title isEqualToString:@"我的问答"]){
//                NSInteger ct = (manager.userModel.new_myanswer + manager.userModel.new_myaccepted + manager.userModel.new_myfabu);
//                if(ct > 0){
//                    model.content = [NSString stringWithFormat:@"%ld",ct];
//                } else {
//                    model.content = @"";
//                }
//            }
//        }
//    }
//    [self.tableView reloadData];
}

#pragma mark - HFStretchableTableHeaderView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_stretchableTableHeaderView scrollViewDidScroll:scrollView];
}

- (void)viewDidLayoutSubviews
{
    [_stretchableTableHeaderView resizeView];
}

#pragma mark - 获取数据
//获取当前界面数据
- (void)getMindList{
    NSData *data = [NSData dataWithContentsOfFile:[NSString mainBundlePath:@"MineList.json"]];
    
    NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    for(int i = 0; i < jsonArray.count;i ++){
        [self.dataArray addObject:[XCMineListModel mj_objectArrayWithKeyValuesArray:jsonArray[i]]];
    }
    
}

 
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArray[section] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XCMineListModel *model = _dataArray[indexPath.section][indexPath.row];
    if(model.isHidden == 0){
        return 62;
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"XCMineListTableViewCell";
    XCMineListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    if([cell.model.title isEqualToString:@"联系客服"]){
        cell.content.backgroundColor = UIColorFromRGB(0xf2f2f2);
        cell.content.textColor = [UIColor whiteColor];
    }else{
        cell.content.backgroundColor = UIColorFromRGB(0xff0000);
        cell.content.textColor = [UIColor whiteColor];
    }
    cell.model = _dataArray[indexPath.section][indexPath.row];

    return cell;
}

#pragma mark UITableViewDelegate 列表点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XCMineListModel *model = _dataArray[indexPath.section][indexPath.row];
    if([model.title isEqualToString:@"添加好友"]){
 
        
  
    } else if([model.title isEqualToString:@"我要反馈"]){
        XCFeedBackViewController *vc = [[XCFeedBackViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if([model.title isEqualToString:@"联系客服"]){
        if(model.content){
            openTel(model.content);
        }
    }  else if([model.title isEqualToString:@"退出"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"是否退出当前用户？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            if(btnIndex == 1){
             
            }
        }];
        [alert show];
    }
}

#pragma mark - 重写----设置标题和标注的高度
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 1){
        return 12;
    }
    return 0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor];
    return v;
}

- (void)goMineInfo:(NSInteger)type{
   

    if(_isLogin %2 == 0){
        _isLogin ++;//测试用 以后以网络登录为准

        LoginViewController *vc = [[LoginViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [app_delegate().tabbarVC presentViewController:nav animated:YES completion:nil];

    } else {
        PersonViewController *vc = [[PersonViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
