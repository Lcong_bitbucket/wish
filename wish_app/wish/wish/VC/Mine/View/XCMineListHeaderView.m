//
//  XCMineListHeaderView.m
//  CLM
//
//  Created by cong on 16/12/9.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMineListHeaderView.h"

@implementation XCMineListHeaderView

- (void)awakeFromNib{
    [super awakeFromNib];
    _myMessageLabel.hidden = YES;

    _myMessageLabel.backgroundColor = UIColorFromRGB(0xf24b4b);
    _myMessageLabel.textColor = [UIColor whiteColor];
    _myMessageLabel.textInsets = UIEdgeInsetsMake(2, 4, 2, 4);
    [_avatar whenTouchedUp:^{
        if(self.didClickBlock){
            self.didClickBlock(0);
        }
    }];
    
    [_infoBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(1);
        }
    }];
    
    
    [_myMsgBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(1);
        }
    }];
    
    [_bg_myTopView whenTapped:^{
        if (self.didClickBlock) {
            self.didClickBlock(1);
        }
    }];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


 
@end
