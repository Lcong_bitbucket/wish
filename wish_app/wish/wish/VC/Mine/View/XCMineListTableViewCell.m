//
//  XCMineListTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/8.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMineListTableViewCell.h"

@implementation XCMineListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _content.textInsets = UIEdgeInsetsMake(2, 4, 2, 4);

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCMineListModel *)model{
    _model = model;
    if(model.isHidden){
        self.contentView.hidden = YES;
    } else {
        self.contentView.hidden = NO;
    }
    _icon.image = imageNamed(_model.icon);
    _title.text = _model.title;
    if(_model.content.length > 0){
        _content.text = _model.content;
        _rightIcon.hidden = YES;
        _content.hidden = NO;
    } else {
        _content.text = _model.content;
        _content.hidden = YES;
        _rightIcon.hidden = NO;
        _rightIcon.image = imageNamed(_model.rightIcon);
    }
}

@end
