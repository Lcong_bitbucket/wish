//
//  XCMineListTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/8.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCMineListModel.h"

@interface XCMineListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *content;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@property (nonatomic,strong) XCMineListModel *model;
@end
