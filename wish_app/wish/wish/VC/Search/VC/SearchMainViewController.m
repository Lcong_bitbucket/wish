//
//  SearchMainViewController.m
//  wish
//
//  Created by Lcong on 2017/3/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "SearchMainViewController.h"
#import "SearchOneTableViewCell.h"
#import "SearchThrTableViewCell.h"
#import "SearchSecTableViewCell.h"

#import "SearchTwoTableViewCell.h"
#import "SearchBarView.h"
#import "SearchSectionView.h"

#import "SearchDetailViewController.h"

#import "NearViewController.h"

@interface SearchMainViewController (){
    SearchBarView *_searchBar;
}
@property (nonatomic,strong) NSMutableArray *array1;
@property (nonatomic,strong) NSMutableArray *array2;
@property (nonatomic,strong) NSMutableArray *array3;
@property (nonatomic,strong) NSMutableArray *array4;

@end

@implementation SearchMainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navColor = UIColorFromRGB(0xFB334A);
    self.array1 = [NSMutableArray new];
    self.array2 = [NSMutableArray new];
    self.array3 = [NSMutableArray new];
    self.array4 = [NSMutableArray new];
    
    [self.array1 addObjectsFromArray:@[@"",@"",@"",@""]];
    [self.array2 addObjectsFromArray:@[@"",@"",@"",@""]];
    [self.array3 addObjectsFromArray:@[@"",@"",@"",@""]];
    [self.array4 addObjectsFromArray:@[@"",@"",@"",@""]];
 
     [self defaultConfig:self.tableView];
    
    _searchBar = [[[NSBundle mainBundle] loadNibNamed:@"SearchBarView" owner:self options:nil] lastObject];
    
    self.navigationItem.titleView = _searchBar;
//    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    
    // Do any additional setup after loading the view from its nib.
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 1;
    } else if(section == 1){
        return 1;
    } else if(section == 2){
        return 1;
    } else if(section == 3) {
        return _array4.count;
    }
    return 0;
 }

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        return 190;
    } else if(indexPath.section == 1){
        return 150;
    } else if(indexPath.section == 2){
        return 130;
    } else if(indexPath.section == 3){
        return UITableViewAutomaticDimension;
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        NSString *identifier = @"SearchOneTableViewCell";
        SearchOneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        [cell setDidClickBlock:^{
            SearchDetailViewController *vc = [[SearchDetailViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
         cell.array = self.array1;
        return cell;
    } else if(indexPath.section == 1){
        NSString *identifier = @"SearchSecTableViewCell";
        SearchSecTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
         [cell setDidClickBlock:^{
            SearchDetailViewController *vc = [[SearchDetailViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
         cell.array = self.array2;
         return cell;

    } else if(indexPath.section == 2){
        NSString *identifier = @"SearchThrTableViewCell";
        SearchThrTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        [cell setDidClickBlock:^{
            SearchDetailViewController *vc = [[SearchDetailViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
         cell.array = self.array3;
         return cell;

    } else if(indexPath.section == 3){
        NSString *identifier = @"SearchTwoTableViewCell";
        SearchTwoTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
 
        return cell;

    }
    return nil;
}

#pragma mark - 重写----设置标题和标注的高度
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
     return 40;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    SearchSectionView *v = [[[NSBundle mainBundle] loadNibNamed:@"SearchSectionView" owner:self options:nil] lastObject];
    if(section == 0){
        v.title.text = @"附近的小店";
        v.more.text = @"查看更多";
        v.rightIcon.hidden = NO;
    } else if(section == 1){
        v.title.text = @"热门标签";
        v.more.hidden = YES;
        v.rightIcon.hidden = YES;
    } else if(section == 2){
        v.title.text = @"逛街好去处";
        v.more.hidden = YES;
        v.rightIcon.hidden = YES;
    } else if(section == 3){
        v.title.text = @"购物攻略";
        v.more.hidden = YES;
        v.rightIcon.hidden = YES;
    }
    
    [v whenTapped:^{
        NearViewController *vc = [[NearViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    return v;
}
#pragma mark UITableViewDelegate 列表点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SearchDetailViewController *vc = [[SearchDetailViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
