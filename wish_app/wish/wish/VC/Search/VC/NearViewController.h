//
//  NearViewController.h
//  wish
//
//  Created by Lcong on 2017/3/16.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface NearViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
