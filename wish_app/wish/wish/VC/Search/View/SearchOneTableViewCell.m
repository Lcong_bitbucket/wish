//
//  SearchOneTableViewCell.m
//  wish
//
//  Created by Lcong on 2017/3/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "SearchOneTableViewCell.h"
#import "SearchOneView.h"
 

@implementation SearchOneTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

            
- (void)setArray:(NSMutableArray *)array{
    _array = array;
    [self.scroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for(int i = 0; i < _array.count; i++){
        SearchOneView *oneV = [[[NSBundle mainBundle] loadNibNamed:@"SearchOneView" owner:self options:nil] lastObject];
        oneV.frame = CGRectMake(10*(i+1) + 230*i, 0, 230, 190) ;
        [oneV whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock();
            }
        }];
        [self.scroll addSubview:oneV];
    }
    
    [self.scroll setContentSize:CGSizeMake(10+ 240*_array.count, 190)];
     
}
@end
