//
//  SearchOneView.h
//  wish
//
//  Created by Lcong on 2017/3/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchThrView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *pic;
@property (weak, nonatomic) IBOutlet UILabel *title;

@property (nonatomic,strong) NSDictionary *dic;
@end
