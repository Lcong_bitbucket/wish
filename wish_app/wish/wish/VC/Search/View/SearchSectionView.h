//
//  SearchSectionView.h
//  wish
//
//  Created by Lcong on 2017/3/15.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchSectionView : UIView
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *more;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;

@end
