//
//  SearchOneTableViewCell.m
//  wish
//
//  Created by Lcong on 2017/3/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "SearchThrTableViewCell.h"
 
#import "SearchThrView.h"

@implementation SearchThrTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

            
- (void)setArray:(NSMutableArray *)array{
    _array = array;
    [self.scroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
        for(int i = 0; i < _array.count; i++){
            SearchThrView *oneV = [[[NSBundle mainBundle] loadNibNamed:@"SearchThrView" owner:self options:nil] lastObject];
            oneV.frame = CGRectMake(10*(i+1) +128*i, 0, 128, 130) ;
            [oneV whenTapped:^{
                if(self.didClickBlock){
                    self.didClickBlock();
                }
            }];
            [self.scroll addSubview:oneV];
        }
        [self.scroll setContentSize:CGSizeMake(10 + 138*_array.count, 130)];
   
    
}
@end
