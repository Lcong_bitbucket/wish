//
//  SearchOneTableViewCell.h
//  wish
//
//  Created by Lcong on 2017/3/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchSecTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (nonatomic,copy) void (^didClickBlock)();
 @property (nonatomic,strong) NSMutableArray *array;
@end
