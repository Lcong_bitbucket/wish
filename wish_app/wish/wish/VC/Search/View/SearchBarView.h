//
//  SearchBarView.h
//  wish
//
//  Created by Lcong on 2017/3/15.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBarView : UIView
@property (weak, nonatomic) IBOutlet UITextField *textfield;

@end
