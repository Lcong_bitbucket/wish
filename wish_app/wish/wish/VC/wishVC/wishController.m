//
//  wishController.m
//  wish
//
//  Created by RSTaylor on 17/3/25.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "wishController.h"
#import "RSModel.h"
#import "RSCollectionViewCell.h"
#import "RSWaterFallLayout.h"
#import "SearchBarView.h"
#import "DynamicDetailViewController.h"


// collectionViewCell的重用标识符
static NSString * const shopCellReuseID = @"RSCollectionViewCell";

@interface wishController ()<UICollectionViewDataSource, RSWaterFallLayoutDelegate,UICollectionViewDelegate>

/** 瀑布流view */
@property (nonatomic, weak) UICollectionView *collectionView;

/** shops */
@property (nonatomic, strong) NSMutableArray *shops;

/** 当前页码 */
@property (nonatomic, assign) NSUInteger currentPage;


@end

@implementation wishController

- (NSMutableArray *)shops
{
    if (_shops == nil) {
        _shops = [NSMutableArray array];
    }
    return _shops;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)setupCollectionView
{
    // 创建瀑布流layout
    RSWaterFallLayout *layout = [[RSWaterFallLayout alloc] init];
    // 设置代理
    layout.delegate = self;
    
    //    // 创建瀑布流view
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 110, Screen_Width, Screen_Height - 49-110) collectionViewLayout:layout];
    // 设置数据源
    collectionView.dataSource = self;
    collectionView.delegate = self;
    
    
    [self.view addSubview:collectionView];
    self.collectionView = collectionView;
    
    self.collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    // 注册cell
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RSCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:shopCellReuseID];
    
    // 为瀑布流控件添加下拉加载和上拉加载
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{ // 模拟网络请求延迟
            
            // 清空数据
            [self.shops removeAllObjects];
            
            [self.shops addObjectsFromArray:[self newShops]];
            
            // 刷新数据
            [self.collectionView reloadData];
            
            // 停止刷新
            [self.collectionView.mj_header endRefreshing];
        });
    }];
    // 第一次进入则自动加载
    [self.collectionView.mj_header beginRefreshing];
    
    
    self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{ // 模拟网络请求延迟
            
            [self.shops addObjectsFromArray:[self moreShopsWithCurrentPage:self.currentPage]];
            
            // 刷新数据
            [self.collectionView reloadData];
            
            // 停止刷新
            [self.collectionView.mj_footer endRefreshing];
        });
    }];
}

#pragma mark - 内部方法
- (NSArray *)newShops
{
    return [RSModel mj_objectArrayWithFilename:@"0.plist"];
}

- (NSArray *)moreShopsWithCurrentPage:(NSUInteger)currentPage
{
    // 页码的判断
    if (currentPage == 3) {
        self.currentPage = 0;
    } else {
        self.currentPage++;
    }
    
    NSString *nextPage = [NSString stringWithFormat:@"%lu.plist", (unsigned long)self.currentPage];
    
    return [RSModel mj_objectArrayWithFilename:nextPage];
}


#pragma mark - <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.shops.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // 创建cell
    RSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:shopCellReuseID forIndexPath:indexPath];
    
    // 给cell传递模型
    cell.model = self.shops[indexPath.item];
    
    // 返回cell
    return cell;
}

#pragma mark - <JRWaterFallLayoutDelegate>
/**
 *  返回每个item的高度
 */
- (CGFloat)waterFallLayout:(RSWaterFallLayout *)waterFallLayout heightForItemAtIndex:(NSUInteger)index width:(CGFloat)width
{
    RSModel *shop = self.shops[index];
    CGFloat shopHeight = [shop.h doubleValue];
    CGFloat shopWidth = [shop.w doubleValue];
    if (shopHeight * width / shopWidth <140) {
        return shopHeight * width *1.4 / shopWidth;
    }
    return shopHeight * width / shopWidth;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DynamicDetailViewController *vc = [[DynamicDetailViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
