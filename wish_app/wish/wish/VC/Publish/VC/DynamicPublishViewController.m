//
//  DynamicPublishViewController.m
//  wish
//
//  Created by Lcong on 2017/3/23.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "DynamicPublishViewController.h"

@interface DynamicPublishViewController ()

@end

@implementation DynamicPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customNavigationBarItemWithImageName:nil title:@"返回" isLeft:YES];
    self.title = @"发布预览";
    [self customNavigationBarItemWithImageName:nil title:@"发布" isLeft:NO];

    
    _cycleScroll.localizationImageNamesGroup = _editFilterArray;
     _cycleScroll.autoScroll = NO;
    _cycleScroll.showPageControl = NO;
 
    [SDCycleScrollView clearImagesCache];// 清除缓存。
    
    
    // Do any additional setup after loading the view from its nib.
    
}

- (void)rightBarButtonAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addWishClick:(id)sender {
}
@end
