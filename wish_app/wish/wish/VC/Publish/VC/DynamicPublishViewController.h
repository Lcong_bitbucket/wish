//
//  DynamicPublishViewController.h
//  wish
//
//  Created by Lcong on 2017/3/23.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "SDCycleScrollView.h"
@interface DynamicPublishViewController : XCBaseViewController

@property (weak, nonatomic) IBOutlet SDCycleScrollView *cycleScroll;
@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet UIButton *isAddWishBtn;
- (IBAction)addWishClick:(id)sender;
@property (strong, nonatomic) NSMutableArray *editFilterArray;//编辑效果

@end
