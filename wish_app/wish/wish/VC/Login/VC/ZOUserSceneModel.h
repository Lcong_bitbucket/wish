//
//  UserSceneModel.h
//  ZUIIO
//
//  Created by zuiio on 16/4/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVOSCloudSNS.h"

@interface ZOUserSceneModel : NSObject

- (void)wechatLogin:(AVBooleanResultBlock)block hudInView:(UIView *)view;

- (void)requestSmsCodeWithPhoneNum:(NSString *)phone operationDescription:(NSString *)description withBlock:(AVBooleanResultBlock)block;

- (void)verifySmsCode:(NSString*)code phone:(NSString *)phone withBlock:(AVBooleanResultBlock)block;

- (void)userRegisterWithPhone:(NSString *)phone password:(NSString *)password nickname:(NSString *)nickname withBlock:(AVBooleanResultBlock)block;

- (void)normalLoginWithPhone:(NSString*)phone password:(NSString *)password withBlock:(AVBooleanResultBlock)block;

- (void)requestResetVerifyCode:(NSString *)phone block:(AVBooleanResultBlock)block;

- (void)resetPasswordVerifyCode:(NSString *)verifyCode newPassword:(NSString *)newPassword block:(AVBooleanResultBlock)block;

- (void)changeAvatarWithImageData:(NSData *)imagedata imageName:(NSString *)imageName block:(AVBooleanResultBlock)block;
    
- (void)changePhoneNum:(NSString *)phone block:(AVBooleanResultBlock)block;

- (void)changeNickName:(NSString *)name block:(AVBooleanResultBlock)block;

- (void)changeUserBrief:(NSString *)userBrief block:(AVBooleanResultBlock)block;

- (void)changeGender:(NSString *)gender block:(AVBooleanResultBlock)block;

+ (void)wechatLogout;

+ (void)phoneLogout;

@end
