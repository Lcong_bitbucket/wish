//
//  HelpViewController.m
//  wish
//
//  Created by Lcong on 2017/3/1.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.title = @"帮助与反馈";
    [self customNavigationBarItemWithImageName:nil title:@"发送" isLeft:NO];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction{
    //发送按钮事件
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
