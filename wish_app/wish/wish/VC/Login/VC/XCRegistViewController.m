//
//  XCRegistViewController.m
//  CLM
//
//  Created by cong on 16/12/16.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCRegistViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ZOUserSceneModel.h"
#define maxTime 60


@interface XCRegistViewController ()
{
    NSInteger _curTime;
    NSTimer *timer;
}
@property (strong, nonatomic) ZOUserSceneModel *sceneModel;
@end

@implementation XCRegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.title = @"设置账号和密码";
    
 }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)getCode:(id)sender {
    if(_phoneTF.text.length == 0)
    {
        [XCTools shakeAnimationForView:_phoneTF];
        showHint(@"请输入您的手机号码");
        return;
    }else {
        [self requestCode];
    }
}

- (void)requestCode{
    [AVOSCloud requestSmsCodeWithPhoneNumber:_phoneTF.text callback:^(BOOL succeeded, NSError *error) {
        if (error) {
            showHint(@"发送失败");
        } else {
            showHint(@"发送验证码成功");

            _curTime = maxTime;
            
            timer  = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changetitle) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
            _codeBtn.enabled = NO;
            [_codeBtn setBackgroundColor:UIColorFromRGB(0xa4abb1)];
            [_codeBtn  setTitle:[NSString stringWithFormat:@"重新获取验证码(%ld)",(long)_curTime] forState:UIControlStateDisabled];
        }
    }];
    
}

- (void)changetitle
{
    _curTime --;
    if(_curTime > 0)
    {
          [_codeBtn  setTitle:[NSString stringWithFormat:@"重新获取验证码(%ld)",(long)_curTime] forState:UIControlStateDisabled];
    }
    else
    {
        if(timer && [timer isValid])
            [timer invalidate];
         [_codeBtn setTitle:[NSString stringWithFormat:@"获取验证码"] forState:UIControlStateNormal];
        _codeBtn.backgroundColor=UIColorFromRGB(0x036EB8);
        _codeBtn.enabled = YES;

    }
}


- (IBAction)nextStep:(id)sender {
    
    [AVOSCloud verifySmsCode:_codeTF.text mobilePhoneNumber:_phoneTF.text callback:^(BOOL succeeded, NSError *error) {
        if (error) {
            showHint(error.description);
         } else if (succeeded){
             AVUser* user = [[AVUser alloc] init];
             user.username = self.phoneTF.text;
             user.password = self.passwordTF.text;
             user.mobilePhoneNumber = self.phoneTF.text;;
             [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                 if (error) {
                     showHint(@"注册失败");
                 } else {
                     showHint(@"注册成功");

                    
                 }
             }];

        } else {
            showHint(@"验证码错误");
        }
    }];
    
    
    
}
@end
