//
//  LoginSecViewController.h
//  wish
//
//  Created by Lcong on 2017/3/1.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"

@interface LoginSecViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
- (IBAction)forgetPassword:(id)sender;
- (IBAction)submitLogin:(id)sender;
- (IBAction)helpAction:(id)sender;

@end
