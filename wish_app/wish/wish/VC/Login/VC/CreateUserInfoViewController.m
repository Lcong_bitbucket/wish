//
//  CreateUserInfoViewController.m
//  wish
//
//  Created by Lcong on 2017/3/1.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "CreateUserInfoViewController.h"

@interface CreateUserInfoViewController ()

@end

@implementation CreateUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.title = @"创建个人资料";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
