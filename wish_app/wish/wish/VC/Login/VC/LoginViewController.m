//
//  LoginViewController.m
//  17duu
//
//  Created by RSTaylor on 17/2/14.
//  Copyright © 2017年 RSTaylor. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"

#import "XCRegistViewController.h"
#import "LoginSecViewController.h"

@interface LoginViewController ()<UIScrollViewDelegate>
- (IBAction)loginAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageView;

@property(nonatomic,assign) CGFloat w;
- (IBAction)loginEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *zhuceEvent;
- (IBAction)ZCEVENT:(id)sender;

@property (nonatomic, strong) NSTimer *timer;
@end

@implementation LoginViewController


-(void)viewWillAppear:(BOOL)animated{
    self.hiddenNavBarWhenPush = YES;
    
    [super viewWillAppear:animated];
    
 
}


-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
   // self.navigationController.navigationBarHidden = NO;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self startUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)startUI{
    
    int count = 3;
    
    CGSize size = self.scrollView.frame.size;
     _w = self.scrollView.frame.size.width;
    
    for (int i = 0; i < count; i++) {
        //
        UIImageView *iconView = [[UIImageView alloc] init];
        [self.scrollView addSubview:iconView];
        
        NSString *imgName = [NSString stringWithFormat:@"图%d",i];
        iconView.image = [UIImage imageNamed:imgName];
        
        
        CGFloat x = i * _w;
        //frame
        iconView.frame = CGRectMake(x, 0, _w, size.height);
    }
    
    //2 设置滚动范围
    self.scrollView.contentSize = CGSizeMake(count * _w, 0);
    
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    //3 设置分页
    self.scrollView.pagingEnabled = YES;
    
    //4 设置pageControl
    self.pageView.numberOfPages = count;
    
    //5 设置scrollView的代理
    self.scrollView.delegate = self;
    
    //6 定时器
    [self addTimerO];
    



}


- (void)addTimerO
{
    NSTimer *timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
    self.timer = timer;
    //消息循环
    NSRunLoop *runloop = [NSRunLoop currentRunLoop];
    [runloop addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)nextImage
{
    //当前页码
    NSInteger page = self.pageView.currentPage;
    
    if (page == self.pageView.numberOfPages - 1) {
        page = 0;
    }else{
        page++;
    }
    
    //    self.scrollView setContentOffset:<#(CGPoint)#> animated:<#(BOOL)#>
    
    // CGFloat offsetX = page * self.scrollView.frame.size.width;
    CGFloat offsetX = page * _w;
    [UIView animateWithDuration:1.0 animations:^{
        self.scrollView.contentOffset = CGPointMake(offsetX, 0);
    }];
    
}

#pragma mark - scrollView的代理方法
//正在滚动的时候
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //   (offset.x + 100/2)/100
    int page = (scrollView.contentOffset.x + scrollView.frame.size.width / 2)/ scrollView.frame.size.width;
    self.pageView.currentPage = page;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //停止定时器
    [self.timer invalidate];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self addTimerO];
    //    self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
}



- (IBAction)loginAction:(id)sender {
    
//    RSViewController *RStabar = [[RSViewController alloc]init];
//    
//    //设置为window的根控制器
//    
//    ((AppDelegate *)[[UIApplication sharedApplication] delegate] ).window.rootViewController=RStabar;
    LoginSecViewController *vc = [[LoginSecViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (IBAction)ZCEVENT:(id)sender {
    
    XCRegistViewController *vc = [[XCRegistViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:vc animated:YES];

 }
- (IBAction)back:(id)sender {
    [self goBack];
}
@end
