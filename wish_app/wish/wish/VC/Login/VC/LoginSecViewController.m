//
//  LoginSecViewController.m
//  wish
//
//  Created by Lcong on 2017/3/1.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "LoginSecViewController.h"
#import "XCForgetPasswordViewController.h"
#import "HelpViewController.h"

@interface LoginSecViewController ()

@end

@implementation LoginSecViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customNavigationBarItemWithImageName:@"形状-20-拷贝" title:nil isLeft:YES];
    self.title = @"登录";
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)forgetPassword:(id)sender {
    XCForgetPasswordViewController *vc = [[XCForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)submitLogin:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (IBAction)helpAction:(id)sender {
    HelpViewController *vc = [[HelpViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
