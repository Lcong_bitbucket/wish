//
//  UserSceneModel.m
//  ZUIIO
//
//  Created by zuiio on 16/4/26.
//  Copyright © 2016年 zuiio. All rights reserved.
//

#import "ZOUserSceneModel.h"
//#import "AVUser+ZOUser.h"
//#import "CDCacheManager.h"
//#import "ZOGlobal.h"

@implementation ZOUserSceneModel


- (BOOL)checkWechatAvailable{
    return [AVOSCloudSNS isAppInstalledForType:AVOSCloudSNSWeiXin];
}

- (void)normalLoginWithPhone:(NSString*)phone password:(NSString *)password withBlock:(AVBooleanResultBlock)block{
    [AVUser logInWithUsernameInBackground:phone password:password block:^(AVUser *user, NSError *error) {
        if (error) {
            block(NO,error);
        }else{
            block(YES,nil);
        }
    }];
}

//- (void)wechatLogin:(AVBooleanResultBlock)block hudInView:(UIView *)view{
//    if ([self checkWechatAvailable]) {
//        [AVOSCloudSNS loginWithCallback:^(id object, NSError *error) {
//            if (error) {
//                block(NO,error);
//            }else{
//                NSString *nickname = object[@"username"];
//                NSString *avatarUrl = object[@"avatar"];
//                [ZOLoading showLoadingInView:view];
//                [AVUser loginWithAuthData:object platform:AVOSCloudSNSPlatformWeiXin block:^(AVUser *user, NSError *error) {
//                    if (error) {
//                        [ZOLoading hideLoadingInView:view];
//                        block(NO,error);
//                    }else{
//                        if (user.nickname) {
//                            [ZOLoading hideLoadingInView:view];
//                            block(YES,nil);
//                        }else{
//                            //上传头像和修改nickname,首次
//                            [self uploadAvatarWithUrl:avatarUrl block:^(AVFile *file, NSError *error) {
//                                if (file) {
//                                    user.avatar = file;
//                                }
//                                user.nickname = nickname;
//                                [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                                    [ZOLoading hideLoadingInView:view];
//                                    block(succeeded,error);
//                                }];
//                            }];
//                        }
//                    }
//                }];
//            }
//        } toPlatform:AVOSCloudSNSWeiXin];
//    }else{
//         [[[UIAlertView alloc] initWithTitle:nil message:@"尚未安装微信客户端." delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
//    }
//}

- (void)requestSmsCodeWithPhoneNum:(NSString *)phone operationDescription:(NSString *)description withBlock:(AVBooleanResultBlock)block{
    [AVOSCloud requestSmsCodeWithPhoneNumber:phone
                                     appName:@"Fave"
                                   operation:description timeToLive:10
                                    callback:block];
   // [AVUser requestMobilePhoneVerify:phone withBlock:block];
}

- (void)verifySmsCode:(NSString*)code phone:(NSString *)phone withBlock:(AVBooleanResultBlock)block{
    [AVOSCloud verifySmsCode:code
           mobilePhoneNumber:phone
                    callback:block];
   // [AVUser requestMobilePhoneVerify:code withBlock:block];
}

- (void)userRegisterWithPhone:(NSString *)phone password:(NSString *)password nickname:(NSString *)nickname withBlock:(AVBooleanResultBlock)block{
    AVUser *registerUser = [AVUser user];
    registerUser.username = phone;
    registerUser.password = password;
    //registerUser.nickname = nickname;
    registerUser.mobilePhoneNumber = phone;
    [registerUser signUpInBackgroundWithBlock:block];
}

- (void)uploadAvatarWithUrl:(NSString *)url block:(AVFileResultBlock)block{
    if (!url) {
        block(nil,nil);
    }else{
        AVFile *file = [AVFile fileWithURL:url];
        [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                block(nil,error);
            }else{
                block(file,nil);
            }
        }];
    }
}

- (void)requestResetVerifyCode:(NSString *)phone block:(AVBooleanResultBlock)block{
    [AVUser requestPasswordResetWithPhoneNumber:phone block:block];
}

- (void)resetPasswordVerifyCode:(NSString *)verifyCode newPassword:(NSString *)newPassword block:(AVBooleanResultBlock)block{
    [AVUser resetPasswordWithSmsCode:verifyCode newPassword:newPassword block:block];
}

//- (void)changePhoneNum:(NSString *)phone block:(AVBooleanResultBlock)block{
//    AVUser *user = [AVUser currentUser];
//    [self countUserByUsername:phone block:^(NSInteger number, NSError *error) {
//        if (error) {
//            block(NO, error);
//        } else {
//            if (number > 0) {
//                NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"手机号已被注册,若忘记密码,可以通过找回密码找回"                                                                      forKey:NSLocalizedDescriptionKey];
//                NSError *t_error = [NSError errorWithDomain:@"com.zuiio.zuiio" code:250 userInfo:userInfo];
//                block(NO,t_error);
//            } else {
//                if ([ZOGlobal sharedGlobal].isPhoneLogin) {
//                    [user setObject:phone forKey:@"username"];
//                }
//                [user setObject:phone forKey:@"mobilePhoneNumber"];
//                [user saveInBackgroundWithBlock:block];
//            }
//        }
//    }];
//}

- (void)countUserByUsername:(NSString *)username block:(AVIntegerResultBlock)block {
    AVQuery *q = [AVUser query];
    [q whereKey:@"username" equalTo:username];
    [q countObjectsInBackgroundWithBlock:block];
}

- (void)changeNickName:(NSString *)name block:(AVBooleanResultBlock)block{
    AVUser *user = [AVUser currentUser];
    [user setObject:name forKey:@"nickname"];
    [user saveInBackgroundWithBlock:block];
}

- (void)changeUserBrief:(NSString *)userBrief block:(AVBooleanResultBlock)block{
    AVUser *user = [AVUser currentUser];
    [user setObject:userBrief forKey:@"userBrief"];
    [user saveInBackgroundWithBlock:block];
}

- (void)changeGender:(NSString *)gender block:(AVBooleanResultBlock)block{
    AVUser *user = [AVUser currentUser];
    [user setObject:gender forKey:@"gender"];
    [user saveInBackgroundWithBlock:block];
}

//- (void)changeAvatarWithImageData:(NSData *)imagedata imageName:(NSString *)imageName block:(AVBooleanResultBlock)block{
//    AVFile *file = [AVFile fileWithName:imageName data:imagedata];
//    AVUser *user = [AVUser currentUser];
//    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        if (error) {
//            block(NO, error);
//        } else {
//            user.avatar = file;
//            [user saveInBackgroundWithBlock:block];
//        }
//    }];
//}

+ (void)wechatLogout{
    [AVOSCloudSNS logout:AVOSCloudSNSWeiXin];
}

+ (void)phoneLogout{
    [AVUser logOut];
}

@end
